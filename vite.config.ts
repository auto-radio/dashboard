import path from 'path'

import { visualizer } from 'rollup-plugin-visualizer'
import vue from '@vitejs/plugin-vue'
import { defineConfig } from 'vite'
import Components from 'unplugin-vue-components/vite'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import svgLoader from 'vite-svg-loader'

import { version } from './package.json'

process.env.VUE_APP_VERSION = version

export default defineConfig({
  base: '/',
  cacheDir: path.resolve(__dirname, 'build', 'vite'),
  clearScreen: false,
  // Traditionally we’ve used the VUE_ prefix for env variables as that was the default
  // environment variable prefix in the vue-cli/webpack based build system.
  // Migrating to the VITE_ prefix might be advisable in the future, but there
  // is no immediate need or benefit in doing so, so we support both.
  envPrefix: ['VUE_', 'VITE_'],
  server: {
    port: 8080,
    strictPort: true,
  },
  build: {
    rollupOptions: {
      input: {
        main: path.resolve(__dirname, 'index.html'),
        oidc_callback: path.resolve(__dirname, 'oidc_callback.html'),
        oidc_callback_silent_renew: path.resolve(__dirname, 'oidc_callback_silentRenew.html'),
      },
    },
  },
  resolve: {
    alias: {
      vue: '@vue/compat',
      '@': path.resolve(__dirname, 'src'),
    },
  },
  plugins: [
    visualizer(),
    vue({
      script: {
        defineModel: true,
      },
      template: {
        compilerOptions: {
          compatConfig: {
            MODE: 2,
          },
        },
      },
    }),
    Components({
      dts: false,
      resolvers: [IconsResolver({ prefix: 'icon' })],
    }),
    Icons({
      scale: 1.4,
    }),
    svgLoader({
      defaultImport: 'url',
      svgoConfig: {
        plugins: [
          {
            name: 'preset-default',
            params: {
              overrides: {
                removeViewBox: false,
              },
            },
          },
        ],
      },
    }),
  ],
})
