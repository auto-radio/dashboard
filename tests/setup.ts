import { chromium, FullConfig } from '@playwright/test'

async function setup(config: FullConfig) {
  const browser = await chromium.launch()
  const page = await browser.newPage()
  const baseURL = config.projects[0].use.baseURL as string
  await page.goto(baseURL)
  await page.getByTestId('login-button').click()
  await page.waitForLoadState('networkidle')
  await page.locator('[name="username"]').fill(process.env.ADMIN_USERNAME ?? 'admin')
  await page.locator('[name="password"]').fill(process.env.ADMIN_PASSWORD ?? 'admin')
  await page.locator('[type="submit"]').click()
  await page.waitForLoadState('networkidle')
  await page.locator('[type="submit"][name="allow"]').click()
  await page.waitForLoadState('networkidle')
  await page.context().storageState({ path: 'session.json' })
  await browser.close()
}

export default setup
