import { expect, test } from '@playwright/test'

const USERNAME = process.env.USERNAME ?? 'admin'
const PASSWORD = process.env.PASSWORD ?? 'admin'

test('Can login and logout again', async ({ browser }) => {
  const context = await browser.newContext({ storageState: undefined })
  const page = await context.newPage()

  await page.goto('/')
  await expect(page).toHaveTitle(/AURA Dashboard/)
  const loginButton = page.getByTestId('login-button')

  await expect(loginButton).toHaveCount(1)
  await loginButton.click()
  await page.waitForLoadState('networkidle')
  await page.locator('[name="username"]').fill(USERNAME)
  await page.locator('[name="password"]').fill(PASSWORD)
  await page.locator('[type="submit"]').click()
  await page.waitForLoadState('networkidle')
  await expect(page.getByTestId('username')).toHaveText(USERNAME)
  await expect(loginButton).toHaveCount(0)

  await page.getByTestId('username').click()
  await page.getByTestId('menu-logout').click()
  await page.waitForLoadState('networkidle')
  await expect(loginButton).toHaveCount(1)
})

test('Is already logged in by default.', async ({ page }) => {
  await page.goto('/')
  await expect(page.getByTestId('username')).toHaveText(USERNAME)
  await expect(page.getByTestId('login-button')).toHaveCount(0)
})
