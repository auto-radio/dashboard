import { expect, test } from '@playwright/test'

test('Can create new show.', async ({ page }) => {
  await page.goto('/')
  await page.getByTestId('nav:shows').click()
  await page.getByTestId('show-selector').locator('input[id^="combobox-input-"]').focus()
  await page.getByTestId('show-selector:add-show').click()
  await page.getByTestId('add-show-modal:show-name').fill('my series')
  await page.getByTestId('add-show-modal:show-description').fill('my series description')
  await page.getByTestId('add-show-modal:show-type').selectOption({ index: 0 })
  await page.getByTestId('add-show-modal:show-funding-category').selectOption({ index: 0 })
  await page.locator('[data-testid="add-show-modal"] button[type="submit"]').click()
  await expect(page.getByTestId('page-header:lead')).toHaveText('my series')
})
