import { defineDefaultAPIStoreOptions } from '@rokoli/bnb/drf'
import BootstrapVue from 'bootstrap-vue'
import { merge } from 'lodash'
import { createPinia } from 'pinia'
import { createApp } from 'vue'
import VueToast from 'vue-toast-notification'
import VueLogger from 'vuejs-logger'

import { TranslationPlugin } from './i18n'
import router from './router'
import store from './store'
import App from './App.vue'

import 'vue-toast-notification/dist/theme-default.css'
import '../node_modules/bootstrap-vue/dist/bootstrap-vue.css'
import '../node_modules/bootstrap/scss/bootstrap.scss'
import './assets/custom.scss'
import './assets/styles/tailwind.css'
import { shouldLog } from '@/utilities'
import { useAuthStore } from '@/stores/auth'

defineDefaultAPIStoreOptions({ mergeDriver: merge })

const pinia = createPinia()
const app = createApp(App)
app.config.warnHandler = (message, instance, trace) => {
  if (shouldLog(message, instance, trace)) {
    console.warn(`[Vue warn]: ${message}`, trace)
  }
}

app.use(store)
app.use(pinia)
app.use(router)
app.use(VueLogger, {
  isEnabled: true,
  logLevel:
    ['debug', 'info', 'warn', 'error', 'fatal'].indexOf(import.meta.env.VUE_APP_LOGLEVEL) === -1
      ? 'warn'
      : import.meta.env.VUE_APP_LOGLEVEL,
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: false,
  separator: '|',
  showConsoleColors: true,
})
app.use(BootstrapVue)
app.use(TranslationPlugin)
app.use(VueToast, { position: 'bottom-left' })
store.$log = app.$log
app.mount('#app')

const authStore = useAuthStore()
void authStore.init()
