import { computed } from 'vue'
import { useI18n } from '@/i18n'

function rruleRender(t, rruleId) {
  return t(`rrule.rule.${rruleId}`)
}

function rruleOptions(t) {
  return [
    { value: 1, text: t('rrule.rule.1') },
    { value: 2, text: t('rrule.rule.2') },
    { value: 3, text: t('rrule.rule.3') },
    { value: 4, text: t('rrule.rule.4') },
    { value: 5, text: t('rrule.rule.5') },
    { value: 6, text: t('rrule.rule.6') },
    { value: 7, text: t('rrule.rule.7') },
    { value: 8, text: t('rrule.rule.8') },
    { value: 9, text: t('rrule.rule.9') },
    { value: 10, text: t('rrule.rule.10') },
    { value: 11, text: t('rrule.rule.11') },
    { value: 12, text: t('rrule.rule.12') },
    { value: 13, text: t('rrule.rule.13') },
  ]
}

export function useRRule() {
  const { t } = useI18n()
  const rruleOptions = computed(() => rruleOptions(t))
  return {
    rruleOptions,
    rruleRender: (rruleID) => rruleRender(t, rruleID),
  }
}

export default {
  computed: {
    rruleOptions() {
      return rruleOptions(this.$t)
    },
  },
  methods: {
    rruleRender(rrule) {
      return rruleRender(this.$t, rrule)
    },
  },
}
