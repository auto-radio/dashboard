import { secondsToDurationString } from '@/util'
import { calculatePlaylistDurationInSeconds } from '@/stores/playlists'

export default {
  methods: {
    playlistDuration(playlist) {
      if (!playlist.entries) {
        return 0
      }

      let delta = 0
      const totalDuration = calculatePlaylistDurationInSeconds(playlist)

      const unknowns = playlist.entries.filter((entry) => !entry.duration)
      if (unknowns.length === 1) {
        delta = this.timeslotDuration - totalDuration
      }

      const totalDurationInSeconds = totalDuration + delta
      return secondsToDurationString(totalDurationInSeconds)
    },
  },
}
