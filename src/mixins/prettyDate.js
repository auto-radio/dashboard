import { useI18n } from '@/i18n'
import { ensureDate } from '@/util'

const day = [
  // static days from sunday to saturday
  // (note: month is zero-indexed)
  new Date(2023, 3, 2),
  new Date(2023, 3, 3),
  new Date(2023, 3, 4),
  new Date(2023, 3, 5),
  new Date(2023, 3, 6),
  new Date(2023, 3, 7),
  new Date(2023, 3, 8),
]

function prettyWeekday(locale, weekday, format = 'long') {
  return day[(weekday + 1) % 7].toLocaleString(locale, { weekday: format })
}

function leadingZero(num) {
  return String(num).padStart(2, '0')
}

export function apiDate(date) {
  const dateObj = ensureDate(date)
  return `${dateObj.getFullYear()}-${leadingZero(dateObj.getMonth() + 1)}-${leadingZero(
    dateObj.getDate(),
  )}`
}

export function getWeekdayFromApiDate(date) {
  const dateObj = ensureDate(date)
  const weekday = dateObj.getDay()
  // Date.getDay() returns 0 for Sundays, but for our APIs we need 0 for Mondays
  if (weekday === 0) {
    return 6
  } else {
    return weekday - 1
  }
}

function prettyDate(locale, date) {
  return ensureDate(date).toLocaleString(locale, {
    weekday: 'long',
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  })
}

function prettyTime(locale, date) {
  return ensureDate(date).toLocaleString(locale, {
    hour: '2-digit',
    minute: '2-digit',
  })
}

function prettyDateTime(locale, date) {
  return ensureDate(date).toLocaleString(locale, {
    weekday: 'long',
    day: 'numeric',
    month: 'long',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
  })
}

export function formatSeconds(durationInSeconds, forceHours = false) {
  const seconds = Math.floor(durationInSeconds % 60)
  const minutes = Math.floor((durationInSeconds % 3600) / 60)
  const hours = Math.floor(durationInSeconds / 3600)

  return hours > 0 || forceHours
    ? `${leadingZero(hours)}:${leadingZero(minutes)}:${leadingZero(seconds)}`
    : `${leadingZero(minutes)}:${leadingZero(seconds)}`
}

export function hmsToSeconds(hms) {
  const parts = hms.split(':').map((t) => parseInt(t))

  if (parts.length > 3 || parts.some((t) => Number.isNaN(t))) {
    throw new TypeError(`Invalid time format: ${hms}`)
  }

  const multiplier = parts.length === 3 ? [60 * 60, 60, 1] : parts.length === 2 ? [60, 1] : [1]
  return parts.reduce((sum, t, index) => sum + t * multiplier[index], 0)
}

const methods = {
  apiDate,
  getWeekdayFromApiDate,
  hmsToSeconds,
  formatSeconds,
}

export function usePretty() {
  const { locale } = useI18n()
  return {
    ...methods,
    prettyDate(date) {
      return prettyDate(locale.value, date)
    },
    prettyTime(date) {
      return prettyTime(locale.value, date)
    },
    prettyDateTime(date) {
      return prettyDateTime(locale.value, date)
    },
    prettyWeekday(weekday) {
      return prettyWeekday(locale.value, weekday)
    },
  }
}

export default {
  methods: {
    ...methods,
    prettyDate(date) {
      return prettyDate(this.$activeLocale(), date)
    },
    prettyTime(date) {
      return prettyTime(this.$activeLocale(), date)
    },
    prettyDateTime(date) {
      return prettyDateTime(this.$activeLocale(), date)
    },
    prettyWeekday(weekday) {
      return prettyWeekday(this.$activeLocale(), weekday)
    },
  },
}
