function compareEpisodesByDate(a, b) {
  const dateA = new Date(a.start)
  const dateB = new Date(b.start)
  if (dateA < dateB) {
    return -1
  } else if (dateA > dateB) {
    return 1
  } else {
    return 0
  }
}

export default {
  computed: {
    timeslotsPast: function () {
      const eps = []
      const now = new Date()
      for (const timeSlot of this.timeslotsSortedDate) {
        if (new Date(timeSlot.start) < now) {
          eps.push(timeSlot)
        }
      }
      return eps
    },
    timeslotsFuture: function () {
      const eps = []
      const now = new Date()
      for (const timeSlot of this.timeslotsSortedDate) {
        if (new Date(timeSlot.start) >= now) {
          eps.push(timeSlot)
        }
      }
      return eps
    },
    timeslotsSortedDate: function () {
      return this.timeslots.sort(compareEpisodesByDate)
    },
  },
}
