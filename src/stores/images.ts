import {
  APICreate,
  APIListPaginated,
  APIRemove,
  APIRetrieve,
  APIUpdate,
  createExtendableAPI,
  useObjectFromStore,
} from '@rokoli/bnb/drf'
import { defineStore } from 'pinia'
import { ComputedRef, MaybeRefOrGetter } from 'vue'

import { steeringAuthInit } from '@/stores/auth'
import { createSteeringURL } from '@/api'

export type Image = {
  id: number
  altText: string
  credits: string
  licenseId: number | null
  isUseExplicitlyGrantedByAuthor: boolean
  image: string
  file?: File
  ppoi: string
  width: number | null
  height: number | null
  thumbnails: { width: number; height: number; url: string }[]
}
export type NewImage = Omit<Image, 'id'> & { file: File }
type ReadonlyAttrs = 'id' | 'width' | 'height' | 'thumbnails' | 'file' | 'image'
type ImageCreateData = Omit<Image, ReadonlyAttrs> & { image: File }
type ImageUpdateData = Partial<Omit<Image, ReadonlyAttrs>>

export const useImageStore = defineStore('images', () => {
  const endpoint = createSteeringURL.prefix('images')
  const { api, base } = createExtendableAPI<Image>(endpoint, steeringAuthInit)
  return {
    ...base,
    ...APIListPaginated(api),
    ...APIRetrieve(api),
    ...APICreate<Image, ImageCreateData>(api),
    ...APIUpdate<Image, ImageUpdateData>(api),
    ...APIRemove(api),
  }
})

export function useImage(imageId: MaybeRefOrGetter<number | null>): ComputedRef<Image | null> {
  const { obj } = useObjectFromStore(imageId, useImageStore())
  return obj
}
