import { APIObject, APIStoreOptions, URLBuilder } from '@rokoli/bnb'
import { defineStore } from 'pinia'
import {
  APICreate,
  APIListUnpaginated,
  APIRemove,
  APIRetrieve,
  APIUpdate,
  createExtendableAPI,
} from '@rokoli/bnb/drf'
import { useOnAuthBehaviour } from '@/stores/auth'

export function createUnpaginatedAPIStore<T extends APIObject>(
  storeId: string,
  endpoint: URLBuilder,
  options?: APIStoreOptions,
  behaviour?: { syncOnAuth: boolean },
) {
  return defineStore(storeId, () => {
    const { api, base } = createExtendableAPI<T>(endpoint, options)
    const listOperations = APIListUnpaginated(api)

    if (behaviour?.syncOnAuth) {
      useOnAuthBehaviour(() => void listOperations.list())
    }

    return {
      ...base,
      ...listOperations,
      ...APIRetrieve(api),
      ...APICreate(api),
      ...APIUpdate(api),
      ...APIRemove(api),
    }
  })
}
