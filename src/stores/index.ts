import { createSteeringURL } from '@/api'
import { Category, FundingCategory, Host, Language, MusicFocus, Topic, Type } from '@/types'
import { steeringAuthInit } from '@/stores/auth'
import { createUnpaginatedAPIStore } from '@/stores/util'

export { useAuthStore, useUserStore } from '@/stores/auth'
export { useImageStore } from '@/stores/images'
export { useLicenseStore } from '@/stores/licenses'
export { useNoteStore } from '@/stores/notes'
export { usePlaylistStore } from '@/stores/playlists'
export { useFilesStore } from '@/stores/files'
export { usePlayoutStore } from '@/stores/playout'
export { useScheduleStore } from '@/stores/schedules'
export { useShowStore } from '@/stores/shows'
export { useTimeSlotStore } from '@/stores/timeslots'

export const useFundingCategoryStore = createUnpaginatedAPIStore<FundingCategory>(
  'fundingCategories',
  createSteeringURL.prefix('funding-categories'),
  steeringAuthInit,
  { syncOnAuth: true },
)
export const useCategoryStore = createUnpaginatedAPIStore<Category>(
  'categories',
  createSteeringURL.prefix('categories'),
  steeringAuthInit,
  { syncOnAuth: true },
)
export const useLanguageStore = createUnpaginatedAPIStore<Language>(
  'languages',
  createSteeringURL.prefix('languages'),
  steeringAuthInit,
  { syncOnAuth: true },
)
export const useTopicStore = createUnpaginatedAPIStore<Topic>(
  'topics',
  createSteeringURL.prefix('topics'),
  steeringAuthInit,
  { syncOnAuth: true },
)
export const useTypeStore = createUnpaginatedAPIStore<Type>(
  'types',
  createSteeringURL.prefix('types'),
  steeringAuthInit,
  { syncOnAuth: true },
)
export const useMusicFocusStore = createUnpaginatedAPIStore<MusicFocus>(
  'musicFocuses',
  createSteeringURL.prefix('music-focus'),
  steeringAuthInit,
  { syncOnAuth: true },
)
export const useHostStore = createUnpaginatedAPIStore<Host>(
  'hosts',
  createSteeringURL.prefix('hosts'),
  steeringAuthInit,
  { syncOnAuth: true },
)
