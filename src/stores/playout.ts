import { defineStore } from 'pinia'
import { computed, MaybeRefOrGetter, toValue } from 'vue'

type Input = {
  uri: string
  label: string
  description?: string
}

export function useInput(uri: MaybeRefOrGetter<string>) {
  const playoutStore = usePlayoutStore()
  return computed(() => playoutStore.inputs.find((i) => i.uri === toValue(uri)) ?? null)
}

export const usePlayoutStore = defineStore('playout', () => {
  // These are hard-coded in engine-core as well
  // See playout config: https://gitlab.servus.at/aura/engine-core/-/blob/40e7c86373a0029e4db9fd98295997017a92a9e3/config/sample.engine-core.ini#L73
  // see playout source: https://gitlab.servus.at/aura/engine-core/-/blob/40e7c86373a0029e4db9fd98295997017a92a9e3/src/in_soundcard.liq#L83-105
  const inputs: Input[] = [
    { uri: 'line://0', label: 'Studio' },
    { uri: 'line://1', label: 'Pre-Production' },
    { uri: 'line://2', label: 'Line 2' },
  ]

  return {
    inputs,
  }
})
