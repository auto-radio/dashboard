import {
  APIListPaginated,
  APIRemove,
  APIRetrieve,
  APIUpdate,
  createExtendableAPI,
} from '@rokoli/bnb/drf'
import { defineStore } from 'pinia'

import { createSteeringURL } from '@/api'
import { components } from '@/steering-types'
import { steeringAuthInit } from '@/stores/auth'
import { KeysFrom, TimeSlot } from '@/types'

type ReadonlyAttrs = KeysFrom<TimeSlot, 'id'>
type TimeSlotUpdateData = Omit<TimeSlot, ReadonlyAttrs>
type TimeSlotPartialUpdateData = components['schemas']['PatchedTimeSlot']

export const useTimeSlotStore = defineStore('timeslots', () => {
  const endpoint = createSteeringURL.prefix('timeslots')
  const { api, base } = createExtendableAPI<TimeSlot>(endpoint, steeringAuthInit)
  return {
    ...base,
    ...APIListPaginated(api),
    ...APIRetrieve(api),
    ...APIUpdate<TimeSlot, TimeSlotUpdateData, TimeSlotPartialUpdateData>(api),
    ...APIRemove(api),
  }
})
