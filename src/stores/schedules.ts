import {
  APIListPaginated,
  APIRemove,
  APIRetrieve,
  APIUpdate,
  createExtendableAPI,
} from '@rokoli/bnb/drf'
import { defineStore } from 'pinia'

import { createSteeringURL } from '@/api'
import { steeringAuthInit } from '@/stores/auth'
import { Schedule } from '@/types'
import { components } from '@/steering-types'

type ScheduleCreateUpdateRequest = components['schemas']['ScheduleCreateUpdateRequest']

export const useScheduleStore = defineStore('schedules', () => {
  const endpoint = createSteeringURL.prefix('schedules')
  const { api, base } = createExtendableAPI<Schedule>(endpoint, steeringAuthInit)
  const { update } = APIUpdate<Schedule, ScheduleCreateUpdateRequest>(api)
  return {
    ...base,
    update,
    ...APIListPaginated(api),
    ...APIRetrieve(api),
    ...APIRemove(api),
  }
})
