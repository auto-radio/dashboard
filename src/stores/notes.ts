import {
  APICreate,
  APIListPaginated,
  APIRemove,
  APIRetrieve,
  APIUpdate,
  createExtendableAPI,
} from '@rokoli/bnb/drf'
import { defineStore } from 'pinia'

import { createSteeringURL } from '@/api'
import { steeringAuthInit } from '@/stores/auth'
import { Note, Show } from '@/types'

type ReadonlyAttrs = 'id' | 'createdAt' | 'createdBy' | 'updatedAt' | 'updatedBy' | 'ownerId'
export type NewNote = Omit<Note, ReadonlyAttrs>
type NoteCreateData = Omit<Note, ReadonlyAttrs>
type NoteUpdateData = Partial<Omit<Note, ReadonlyAttrs>>

export function newNote(timeslotId: number, show?: Show): NewNote {
  return {
    timeslotId,
    title: '',
    slug: '',
    summary: '',
    content: '',
    imageId: null,
    cbaId: null,
    contributorIds: show?.hostIds ?? [],
    languageIds: [],
    topicIds: [],
    links: [],
    tags: [],
  }
}

export const useNoteStore = defineStore('notes', () => {
  const endpoint = createSteeringURL.prefix('notes')
  const { api, base } = createExtendableAPI<Note>(endpoint, steeringAuthInit)
  return {
    ...base,
    ...APIListPaginated(api),
    ...APIRetrieve(api),
    ...APICreate<Note, NoteCreateData>(api),
    ...APIUpdate<Note, NoteUpdateData>(api),
    ...APIRemove(api),
  }
})
