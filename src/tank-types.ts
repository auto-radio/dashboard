/* eslint-disable */
/*
 * This file was auto-generated by `make update-types` at 2023-10-13 19:41:45.807Z.
 * DO NOT make changes to this file.
 */

export interface paths {
  '/api/v1/playlists': {
    /**
     * List playlists
     * @description Lists all playlists.
     */
    get: {
      parameters: {
        query?: {
          /** @description Limit number of results */
          limit?: number
          /** @description Start listing from offset */
          offset?: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['v1.PlaylistsListing']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/playlists/{id}': {
    /**
     * Retrieve playlist
     * @description Retrieves a playlist.
     */
    get: {
      parameters: {
        path: {
          /** @description ID of the playlist */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['store.Playlist']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows': {
    /**
     * List shows
     * @description Lists all existing shows
     */
    get: {
      parameters: {
        query?: {
          /** @description Limit number of results */
          limit?: number
          /** @description Start listing from offset */
          offset?: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['v1.ShowsListing']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}': {
    /**
     * Create show
     * @description Creates a new show
     */
    post: {
      parameters: {
        query?: {
          /** @description If given, all files and playlists will be copied from the show */
          cloneFrom?: string
        }
        path: {
          /** @description Name of the show to be created */
          name: string
        }
      }
      responses: {
        /** @description Created */
        201: {
          content: {
            'application/json': components['schemas']['store.Show']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Conflict */
        409: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Delete show
     * @description Deletes a show
     */
    delete: {
      parameters: {
        path: {
          /** @description Name of the show to be deleted */
          name: string
        }
      }
      responses: {
        /** @description No Content */
        204: {
          content: {}
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/files': {
    /**
     * List files
     * @description Lists files of show
     */
    get: {
      parameters: {
        query?: {
          /** @description Limit number of results */
          limit?: number
          /** @description Start listing from offset */
          offset?: number
        }
        path: {
          /** @description Name of the show */
          name: string
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['v1.FilesListing']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Add file
     * @description Adds a file and starts import
     */
    post: {
      parameters: {
        query?: {
          /** @description running|done - If given, return not before import has the given state */
          waitFor?: string
        }
        path: {
          /** @description Name of the show */
          name: string
        }
      }
      /** @description URI of the file */
      requestBody: {
        content: {
          'application/json': components['schemas']['v1.FileCreateRequest']
        }
      }
      responses: {
        /** @description Created */
        201: {
          content: {
            'application/json': components['schemas']['store.File']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/files/{id}': {
    /**
     * Retrieve file
     * @description Retrieves file object.
     */
    get: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['store.File']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Delete file
     * @description Removes a file.
     */
    delete: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description No Content */
        204: {
          content: {}
        }
        /** @description Bad Request */
        400: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Conflict */
        409: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Update file
     * @description Updates file metadata.
     */
    patch: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      /** @description File metadata */
      requestBody?: {
        content: {
          'application/json': components['schemas']['store.FileMetadata']
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['store.File']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/files/{id}/import': {
    /**
     * Retrieve import status
     * @description Retrieves import status of the file.
     */
    get: {
      parameters: {
        query?: {
          /** @description running|done - If given, return not before import has the given state */
          waitFor?: string
        }
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['importer.Job']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description No job for this file */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Cancel file import
     * @description Cancels import of file.
     */
    delete: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description No Content */
        204: {
          content: {}
        }
        /** @description Bad Request */
        400: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description No job for this file */
        404: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/files/{id}/logs': {
    /**
     * Retrieve import logs
     * @description Retrieves import logs of the file.
     */
    get: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['v1.FileImportLogs']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/files/{id}/upload': {
    /**
     * Upload file content
     * @description Uploads file content. Only available if file was created using SourceURI set to upload://-type and file import state is running.
     */
    get: {
      parameters: {
        query: {
          /** @description A unique identifier for the uploaded file */
          flowIdentifier: string
        }
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {}
        }
        /** @description No Content */
        204: {
          content: {}
        }
        /** @description Bad Request */
        400: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Conflict */
        409: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Upload file content
     * @description Uploads file content. Only available if file was created using SourceURI set to upload://-type and file import state is running.
     */
    put: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {}
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Upload file content
     * @description Uploads file content via flow.js. Only available if file was created using SourceURI set to upload://-type and file import state is running.
     */
    post: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {}
        }
        /** @description Bad Request */
        400: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Conflict */
        409: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/files/{id}/usage': {
    /**
     * List referring playlists
     * @description Lists playlists referring to the file.
     */
    get: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the file */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['v1.FileUsageListing']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/imports': {
    /**
     * List imports
     * @description Lists all running and pending imports
     */
    get: {
      parameters: {
        query?: {
          /** @description Limit number of results */
          limit?: number
          /** @description Start listing from offset */
          offset?: number
        }
        path: {
          /** @description Name of the show */
          name: string
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['v1.JobsListing']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/playlists': {
    /**
     * List playlists
     * @description Lists playlists of show.
     */
    get: {
      parameters: {
        query?: {
          /** @description Limit number of results */
          limit?: number
          /** @description Start listing from offset */
          offset?: number
        }
        path: {
          /** @description Name of the show */
          name: string
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['v1.PlaylistsListing']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Create playlist
     * @description Creates a new playlist for the show.
     */
    post: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
        }
      }
      /** @description Playlist data */
      requestBody: {
        content: {
          'application/json': components['schemas']['store.Playlist']
        }
      }
      responses: {
        /** @description Created */
        201: {
          content: {
            'application/json': components['schemas']['store.Playlist']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/api/v1/shows/{name}/playlists/{id}': {
    /**
     * Retrieve playlist
     * @description Retrieves a playlist of a show.
     */
    get: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the playlist */
          id: number
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['store.Playlist']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Update playlist
     * @description Updates a playlist of a show.
     */
    put: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the playlist */
          id: number
        }
      }
      /** @description Playlist data */
      requestBody: {
        content: {
          'application/json': components['schemas']['store.Playlist']
        }
      }
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['store.Playlist']
          }
        }
        /** @description Bad Request */
        400: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            'application/json': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
    /**
     * Delete playlist
     * @description Deletes a playlist of a show.
     */
    delete: {
      parameters: {
        path: {
          /** @description Name of the show */
          name: string
          /** @description ID of the playlist */
          id: number
        }
      }
      responses: {
        /** @description No Content */
        204: {
          content: {}
        }
        /** @description Bad Request */
        400: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Forbidden */
        403: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Not Found */
        404: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
        /** @description Internal Server Error */
        500: {
          content: {
            '*/*': components['schemas']['v1.ErrorResponse']
          }
        }
      }
    }
  }
  '/healthz': {
    /**
     * Check health
     * @description Checks daemon health.
     */
    get: {
      responses: {
        /** @description OK */
        200: {
          content: {
            'application/json': components['schemas']['main.Health']
          }
        }
        /** @description Service Unavailable */
        503: {
          content: {
            'application/json': components['schemas']['main.Health']
          }
        }
      }
    }
  }
}

export type webhooks = Record<string, never>

export interface components {
  schemas: {
    'importer.Job': {
      created?: string
      id?: number
      progress?: number
      refID?: string
      show?: string
      source?: components['schemas']['importer.SourceURL']
      started?: number
      state?: components['schemas']['importer.JobState']
      user?: string
    }
    /** @enum {integer} */
    'importer.JobState': 0 | 1 | 2 | 3 | 4 | 5
    'importer.SourceURL': string
    'main.Health': {
      auth?: string
      importer?: string
      store?: string
    }
    'store.File': {
      created?: string
      duration?: number
      id?: number
      metadata?: components['schemas']['store.FileMetadata']
      show?: string
      size?: number
      source?: components['schemas']['store.FileSource']
      updated?: string
    }
    'store.FileMetadata': {
      album?: string
      /** @description actually a full-text index would be nice here... */
      artist?: string
      isrc?: string
      organization?: string
      title?: string
    }
    'store.FileSource': {
      hash?: string
      import?: components['schemas']['store.Import']
      uri?: string
    }
    'store.Import': {
      error?: string
      state?: string
    }
    'store.ImportLogs': {
      [key: string]: components['schemas']['store.Log']
    }
    'store.Log': Record<string, never>
    'store.Playlist': {
      created?: string
      description?: string
      entries?: components['schemas']['store.PlaylistEntry'][]
      id?: number
      playoutMode?: string
      show?: string
      updated?: string
    }
    'store.PlaylistEntry': {
      duration?: number
      file?: components['schemas']['store.File']
      uri?: string
    }
    'store.Show': {
      created?: string
      name?: string
      updated?: string
    }
    'url.Userinfo': Record<string, never>
    'v1.ErrorResponse': {
      detail?: Record<string, never>
      error?: string
    }
    'v1.FileCreateRequest': {
      /** @enum {string} */
      sourceURI?: 'upload://<filename>' | 'https://<host>/<path>' | 'file:///<path>'
    }
    'v1.FileImportLogs': {
      results?: components['schemas']['store.ImportLogs']
    }
    'v1.FileUsageListing': {
      results?: components['schemas']['v1_FileUsageListing_results']
    }
    'v1.FilesListing': {
      results?: components['schemas']['store.File'][]
    }
    'v1.JobsListing': {
      results?: components['schemas']['importer.Job'][]
    }
    'v1.PlaylistsListing': {
      results?: components['schemas']['store.Playlist'][]
    }
    'v1.ShowsListing': {
      results?: components['schemas']['store.Show'][]
    }
    v1_FileUsageListing_results: {
      playlists?: components['schemas']['store.Playlist'][]
    }
  }
  responses: never
  parameters: never
  requestBodies: never
  headers: never
  pathItems: never
}

export type $defs = Record<string, never>

export type external = Record<string, never>

export type operations = Record<string, never>
