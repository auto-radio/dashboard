import axios from 'axios'
import { handleApiError } from '../api-helper'
import { createTankURL } from '@/api'

const state = {
  playlists: [],
  loaded: {
    playlists: false,
  },
}

const mutations = {
  loading(state, item) {
    state.loaded[item] = false
  },
  finishLoading(state, item) {
    state.loaded[item] = true
  },

  setPlaylists(state, lists) {
    state.playlists = lists
  },
  addPlaylist(state, list) {
    state.playlists.push(list)
  },
  updatePlaylist(state, data) {
    const i = state.playlists.findIndex((p) => p.id === data.id)
    if (i >= 0) {
      state.playlists[i] = data.playlist
    } else {
      console.log('[ERROR] updatePlaylist: trying to update a non-existing list')
    }
  },
  deletePlaylist(state, id) {
    const i = state.playlists.findIndex((p) => p.id === id)
    if (i >= 0) {
      state.playlists.splice(i, 1)
    } else {
      console.log('[ERROR] deletePlaylist: trying to delete a non-existing list')
    }
  },
}

const getters = {
  playlists: (state) => state.playlists,
  playlistCount: (state) => state.playlists.length,
  getPlaylistById: (state) => (id) => {
    let list
    if (id !== undefined) {
      list = state.playlists.find((l) => l.id == id)

      if (list === undefined) {
        console.log('[ERROR] getPlaylistById: ID not found in store!')
      }
    }
    return list
  },
}

const actions = {
  fetch(ctx, data) {
    ctx.commit('loading', 'playlists')
    const uri = createTankURL('shows', data.showSlug, 'playlists')
    axios
      .get(uri)
      .then((response) => {
        // we don't have to check separately, if there are playlists, because tank
        // always provides an empty array if there are no playlists (or even if there is no corresponding show)
        ctx.commit('setPlaylists', response.data.results)
        ctx.commit('finishLoading', 'playlists')
        if (data && typeof data.callback === 'function') {
          data.callback()
        }
      })
      .catch((error) => {
        handleApiError(this, error, 'could not fetch playlists from tank')
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },

  add(ctx, data) {
    const uri = createTankURL('shows', data.showSlug, 'playlists')
    axios
      .post(uri, data.playlist)
      .then((response) => {
        ctx.commit('addPlaylist', response.data)
        if (data && typeof data.callback === 'function') {
          data.callback(response.data)
        }
      })
      .catch((error) => {
        handleApiError(this, error, 'could not add new playlist')
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },

  update(ctx, data) {
    const uri = createTankURL('shows', data.showSlug, 'playlists', data.playlistId)
    axios
      .put(uri, data.playlist)
      .then((response) => {
        ctx.commit('updatePlaylist', {
          id: data.playlistId,
          playlist: response.data,
        })
        if (data && typeof data.callback === 'function') {
          data.callback()
        }
      })
      .catch((error) => {
        handleApiError(this, error, 'could not update playlist')
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },

  delete(ctx, data) {
    const uri = createTankURL('shows', data.showSlug, 'playlists', data.playlistId)
    axios
      .delete(uri)
      .then(() => {
        ctx.commit('deletePlaylist', data.playlistId)
        if (data && typeof data.callback === 'function') {
          data.callback()
        }
      })
      .catch((error) => {
        handleApiError(this, error, 'could not delete playlist')
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
