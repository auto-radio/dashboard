import axios from 'axios'
import { handleApiError } from '../api-helper'
import { createTankURL } from '@/api'

const state = {
  files: [],
  log: [],
  loaded: {
    files: false,
    log: false,
  },
}

const getters = {
  files: (state) => state.files,
  fileCount: (state) => state.files.length,
  log: (state) => state.log,
  getFileById: (state) => (id) => {
    let file
    if (id !== undefined) {
      file = state.files.find((f) => f.id === id)
      if (file === undefined) {
        console.log('[ERROR] getFileById: ID not found in store!')
      }
    }
    return file
  },
}

const mutations = {
  loading(state, item) {
    state.loaded[item] = false
  },
  finishLoading(state, item) {
    state.loaded[item] = true
  },

  setFiles(state, files) {
    state.files = files
  },
  addFile(state, file) {
    state.files.push(file)
  },
  setFileMeta(state, data) {
    const file = state.files.find((f) => f.id === data.id)
    file.metadata = data.metadata
  },
  deleteFile(state, id) {
    const i = state.files.findIndex((f) => f.id === id)
    if (i >= 0) {
      state.files.splice(i, 1)
    }
  },

  setLog(state, log) {
    state.log = log
  },

  updateProgress(state, data) {
    const file = state.files.find((f) => f.id == data.id)
    if (file) {
      file.source.import.progress = {
        progress: data.progress,
        step: data.step,
      }
    }
  },
}

const actions = {
  // Fetch all files for a given show from the AuRa tank API
  fetchFiles(ctx, data) {
    ctx.commit('loading', 'files')
    const uri = createTankURL('shows', data.showSlug, 'files')
    axios
      .get(uri)
      .then((response) => {
        // we don't have to check separately, if there are files, because tank
        // always provides an empty array if there are no files (or even if there is no corresponding show)
        ctx.commit('setFiles', response.data.results)
        ctx.commit('finishLoading', 'files')
        if (data && typeof data.callback === 'function') {
          data.callback(response.data.results)
        }
      })
      .catch((error) => {
        handleApiError(this, error, 'could not load files')
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },

  fetchLog(ctx, data) {
    ctx.commit('loading', 'log')
    const uri = createTankURL('shows', data.showSlug, 'files', data.fileId, 'logs')
    axios
      .get(uri)
      .then((response) => {
        // we don't have to check separately, if there are files, because tank
        // always provides an empty array if there are no files (or even if there is no corresponding show)
        ctx.commit('setLog', response.data.results)
        ctx.commit('finishLoading', 'log')
        if (data && typeof data.callback === 'function') {
          data.callback()
        }
      })
      .catch((error) => {
        handleApiError(this, error, 'could not load file log')
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },

  // This function fetches all running imports for a given show. It should
  // be called periodically to reflect the upload/import progress. When no
  // more active imports are available the corresponding updateInterval
  // should be cleared again.
  fetchImports(ctx, data) {
    const uri = createTankURL('shows', data.showSlug, 'imports')
    axios
      .get(uri)
      .then((response) => {
        if (data && typeof data.callback === 'function') {
          data.callback(response)
        }
      })
      .catch((error) => {
        handleApiError(this, error, 'could not fetch current imports')
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },

  updateFile(ctx, data) {
    const uri = createTankURL('shows', data.showSlug, 'files', data.fileId)
    // TODO: add mechanism to indicate the running patch request in the files table
    axios
      .patch(uri, data.metadata)
      .then((response) => {
        ctx.commit('setFileMeta', { id: data.fileId, metadata: response.data.metadata })
        if (data && typeof data.callback === 'function') {
          data.callback()
        }
      })
      .catch((error) => {
        handleApiError(this, error, 'could not update file')
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },

  // With this function we add a new file in the AuRa tank by calling its API.
  // Depending on wheter we add a remote file which tank then imports by itself,
  // or if we want to upload a local file, the sourceURI has to look different.
  // And for uploading a local file this is just the first step. Afterwards the
  // actual upload has to be started with the startUpload function.
  addFile(ctx, data) {
    const uri = createTankURL('shows', data.showSlug, 'files')
    const payload = {}
    if (data.addNewFileURI) {
      payload['sourceURI'] = data.uploadSourceURI
    } else {
      payload['sourceURI'] = encodeURI(encodeURI('upload://' + data.uploadSourceFile.name))
    }
    axios
      .post(uri, payload)
      .then((response) => {
        if (!data.addNewFileURI) {
          ctx.dispatch('startUpload', {
            showSlug: data.showSlug,
            fileId: response.data.id,
            ...data,
          })
        }
        if (data && typeof data.callback === 'function') {
          data.callback()
        }
      })
      .catch((error) => {
        let msg = 'could not add the new remote import'
        if (!data.addNewFileURI) {
          msg = 'could not add the new file upload'
        }
        handleApiError(this, error, msg)
        if (data && typeof data.callbackCancel === 'function') {
          data.callbackCancel()
        }
      })
  },

  // When a new file was added with the addFile dispatch we can start an upload
  // fetching the import endpoint of this file and then call the upload
  // function, which atually puts the file onto the server.
  startUpload(ctx, data) {
    const uri = createTankURL('shows', data.showSlug, 'files', data.fileId, 'import')
    axios
      .get(uri, {
        params: { waitFor: 'running' },
      })
      .then(ctx.dispatch('upload', data))
      .catch((error) => {
        handleApiError(this, error, 'could not start the file upload')
      })
  },

  // Upload a file to the AuRa tank API - given it was created with the addFile
  // and started with the startUpload dispatch.
  upload(ctx, data) {
    /*
         * NOTE: there is no npm package for flow.js and importing it manually did not
         *       work so far. therefore this is commented out and we are using the simple
         *       upload method, until there is a nice npm package for flow.js or somone
         *       resolves this issue otherwise
        let flow = new Flow({
          target: createTankURL('shows', data.show, 'files', data.file, 'upload'),
          chunkSize: 100 * 1024,
          prioritizeFirstAndLastChunk: true
        })
        flow.on('fileSuccess', function(file, message) {
          this.$log.error(file, message)
        })
        flow.on('fileError', function(file, message) {
          this.$log.error(file, message)
          alert('Error: could not upload your file. See console for details.')
        })
        flow.addFile(this.uploadSourceFile)
        flow.upload()
        */

    const uri = createTankURL('shows', data.showSlug, 'files', data.fileId, 'upload')
    axios
      .put(uri, data.uploadSourceFile, {
        headers: {
          'Content-Type': 'application/octet-stream',
        },
      })
      .then(() => {
        this.$log.info('Sucessfully uploaded file: ' + data.uploadSourceFile.name)
      })
      .catch((error) => {
        if (error.response.status === 500 && error.response.data.error === 'ffmpeg returned 1') {
          this.$log.error(error.response.status + ' ' + error.response.statusText)
          this.$log.error(error.response)
          // if we use a file format that is not supported by ffmpeg, we should find
          // the second to last line should notify us about invalid data
          const ffmpegError = error.response.data.detail[error.response.data.detail.length - 2]
          if (ffmpegError.line === 'pipe:: Invalid data found when processing input') {
            // in this case we can make the error message in the files table more specific
            alert('Error: import aborted. The audio data format of your file is not valid!')
          } else {
            alert('Error: ffmpeg could not process your file! See console for details.')
          }
        } else {
          handleApiError(this, error, 'could not finish the file upload/import')
        }
      })
  },

  // Deletes a file with a specific ID calling the AuRa tank API
  deleteFile(ctx, data) {
    const uri = createTankURL('shows', data.showSlug, 'files', data.fileId)
    // TODO: add mechanism to indicate the running delete request in the files table
    axios
      .delete(uri)
      .then(() => {
        ctx.commit('deleteFile', data.fileId)
        if (data && typeof data.callback === 'function') {
          data.callback()
        }
      })
      .catch((error) => {
        // if there was a 409 Conflict response it means, that this file is
        // still used in one or more playlists.
        if (error.response && error.response.status === 409) {
          const pls = error.response.data.detail.playlists.length
          let msg = 'Cannot delete file. Still used in ' + pls + ' playlists:\n\n'
          for (const pl of error.response.data.detail.playlists) {
            msg += 'ID: ' + pl.id
            if (pl.description) {
              msg += ' (' + pl.description + ')'
            }
            msg += '\n'
          }
          msg += '\nIf you want to delete the file, remove it from those playlists first.'
          alert(msg)
        } else {
          handleApiError(this, error, 'could not delete file')
          if (data && typeof data.callbackCancel === 'function') {
            data.callbackCancel()
          }
        }
      })
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
