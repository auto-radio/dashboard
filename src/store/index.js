import { createStore } from 'vuex'
import files from './modules/files'
import playlists from './modules/playlists'
import shows from './modules/shows'

const debug = import.meta.env.DEV === true

const state = {
  fileManagerMode: 'files',
}

const getters = {
  fileManagerMode: (state) => state.fileManagerMode,
}

const actions = {}

const mutations = {
  setFileManagerMode(state, mode) {
    state.fileManagerMode = mode
  },
}

export default new createStore({
  strict: debug,
  modules: {
    shows,
    files,
    playlists,
  },
  state,
  getters,
  actions,
  mutations,
})
