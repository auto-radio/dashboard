import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
const addOrEditPlaylistPage = () => import('@/Pages/AddOrEditPlaylist.vue')

const routes: RouteRecordRaw[] = [
  { path: '/', redirect: '/shows' },
  { path: '/shows', name: 'shows', component: () => import('@/Pages/MyShows.vue') },
  {
    path: '/shows/:showId',
    name: 'show',
    component: () => import('@/Pages/Show.vue'),
    redirect: (to) => ({ path: `/shows/${to.params.showId}/episodes` }),
    children: [
      {
        path: 'basic-data',
        name: 'show-basic-data',
        component: () => import('@/Pages/ShowBasicData.vue'),
      },
      {
        path: 'episodes',
        name: 'show-episodes',
        component: () => import('@/Pages/ShowEpisodes.vue'),
      },
      {
        path: 'episodes/:episodeId',
        name: 'show-episode',
        component: () => import('@/Pages/ShowEpisode.vue'),
        redirect: (to) => ({
          path: `/shows/${to.params.showId}/episodes/${to.params.episodeId}/details`,
        }),
        children: [
          {
            path: 'details',
            name: 'show-episode-details',
            component: () => import('@/Pages/ShowEpisodeDetails.vue'),
          },
        ],
      },
    ],
  },
  { path: '/files', name: 'files', component: () => import('@/Pages/FileManager.vue') },
  { path: '/calendar', name: 'calendar', component: () => import('@/Pages/EmissionManager.vue') },
  { path: '/help', name: 'help', component: () => import('@/Pages/Help.vue') },
  { path: '/settings', name: 'settings', component: () => import('@/Pages/Settings.vue') },
  { path: '/credits', name: 'credits', component: () => import('@/Pages/Credits.vue') },
  {
    path: '/playlist/new',
    name: 'addPlaylist',
    component: addOrEditPlaylistPage,
    props: { id: false },
  },
  { path: '/playlist/:id', name: 'editPlaylist', component: addOrEditPlaylistPage, props: true },
]

export default createRouter({
  history: createWebHistory(),
  routes,
})
