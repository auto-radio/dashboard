import { components as steeringComponents } from '@/steering-types'
import { components as tankComponents } from '@/tank-types'

export type Module = {
  icon: string
  slug: string
  title: string
}

export type PaginationData = {
  count: number
  hasNext: boolean
  hasPrevious: boolean
  page: number
  itemsPerPage: number
  itemRange: [number, number]
}

export type KeysFrom<T, K extends keyof T> = K
export type PickOfType<T, V> = { [K in keyof T as T[K] extends V ? K : never]: T[K] }

export type UpdatedMetadata = {
  updatedAt: string | null
  updatedBy: string
}

export type CreatedMetadata = {
  createdAt: string
  createdBy: string
}

export type EditingMetaData = UpdatedMetadata & CreatedMetadata

export type TimeSlot = Required<steeringComponents['schemas']['TimeSlot']>
export type Show = Required<steeringComponents['schemas']['Show']>
export type Playlist = Required<tankComponents['schemas']['store.Playlist']>
export type PlaylistEntry = Required<tankComponents['schemas']['store.PlaylistEntry']>
export type File = Required<tankComponents['schemas']['store.File']>
export type FileMetadata = Required<tankComponents['schemas']['store.FileMetadata']>
export type Import = Required<tankComponents['schemas']['importer.Job']>
export type Schedule = Required<steeringComponents['schemas']['Schedule']>
export type Host = Required<steeringComponents['schemas']['Host']>
export type Category = Required<steeringComponents['schemas']['Category']>
export type FundingCategory = Required<steeringComponents['schemas']['FundingCategory']>
export type Language = Required<steeringComponents['schemas']['Language']>
export type MusicFocus = Required<steeringComponents['schemas']['MusicFocus']>
export type Type = Required<steeringComponents['schemas']['Type']>
export type Topic = Required<steeringComponents['schemas']['Topic']>
export type License = Required<steeringComponents['schemas']['License']>
export type Note = Required<
  Omit<
    steeringComponents['schemas']['Note'],
    'tags' | 'playlistId' | 'topicIds' | 'languageIds'
  > & {
    tags: string[]
    topicIds: number[]
    languageIds: number[]
  }
>
