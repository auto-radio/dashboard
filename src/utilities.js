export function uppercaseFirst(string = '') {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export function lowercaseFirst(string = '') {
  return string.charAt(0).toLowerCase() + string.slice(1)
}

export function has(obj, prop) {
  try {
    return Object.prototype.hasOwnProperty.call(obj, prop)
  } catch (e) {
    return false
  }
}

export function getISODateString(date) {
  date = date ? date : new Date()
  const year = date.getFullYear()
  const month = (date.getMonth() + 1).toString().padStart(2, '0')
  const day = date.getDate().toString().padStart(2, '0')
  return `${year}-${month}-${day}`
}

export function getTimeString(date, withSeconds = false) {
  date = date ? date : new Date()
  const hours = date.getHours().toString().padStart(2, '0')
  const minutes = date.getMinutes().toString().padStart(2, '0')
  const seconds = date.getSeconds().toString().padStart(2, '0')
  return `${hours}:${minutes}${withSeconds ? `:${seconds}` : ''}`
}

export function shouldLog(message, instance, trace) {
  // don’t log any Bootstrap-Vue warnings
  if (/^B[A-Z]/.test(instance?.$options?.name ?? '')) {
    return false
  }
  if (!instance && /^at <B[A-Z]/.test(trace)) {
    return false
  }
  if (/Functional component <B[A-Z]/.test(message)) {
    return false
  }

  return instance?.$parent ? shouldLog(message, instance.$parent) : true
}
