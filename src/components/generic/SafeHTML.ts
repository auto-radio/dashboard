import { defineComponent, h, PropType } from 'vue'
import { sanitizeHTML } from '@/util'

export default defineComponent({
  props: {
    html: { type: String as PropType<string>, required: true },
    sanitizePreset: {
      type: String as PropType<'inline-noninteractive' | 'safe-html' | 'strip'>,
      required: false,
      default: 'strip',
    },
    as: { type: String as PropType<string | undefined>, required: false, default: undefined },
  },
  setup(props) {
    return () =>
      h(props.as ? props.as : props.sanitizePreset === 'safe-html' ? 'div' : 'span', {
        innerHTML: sanitizeHTML(props.html, props.sanitizePreset),
      })
  },
})
