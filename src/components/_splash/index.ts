import _microphone from './gmuAWJGNHcY.jpg?url'
import _radios from './l_-fdJPwwpc.jpg?url'
import _mixer from './n41Xj-JgauA.jpg?url'

export const microphone = {
  image: _microphone,
  author: 'Austin Neill',
  source: 'https://unsplash.com/de/fotos/bokeh-photography-of-condenser-microphone-gmuAWJGNHcY',
}

export const radios = {
  image: _radios,
  author: 'Miguel Alcântara',
  source:
    'https://unsplash.com/de/fotos/eine-reihe-altmodischer-radios-die-auf-einem-tisch-sitzen-l_-fdJPwwpc',
}

export const mixer = {
  image: _mixer,
  author: 'Bryton Udy',
  source: 'https://unsplash.com/de/fotos/grauer-und-roter-audiomischer-n41Xj-JgauA',
}
