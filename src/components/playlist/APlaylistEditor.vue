<template>
  <div class="tw-relative">
    <SaveIndicator
      v-if="isUpdatingPlaylist"
      class="tw-absolute tw-right-0 -tw-top-7 tw-z-10 -tw-translate-y-full"
      role="alert"
      aria-live="polite"
      :aria-label="t('playlist.editor.isSaving')"
    />

    <APlaylistDurationCheck
      v-if="playlist && playlist.entries.length > 0"
      :playlist="playlist"
      :required-duration-seconds="requiredDurationSeconds"
      class="tw-mb-6"
    />

    <APlaylistEntries v-if="entries.length > 0" v-model:entries="entries" class="tw-mb-6" />

    <div
      v-if="nonAddedEntries.length > 0"
      class="tw-border-2 tw-border-rose-200 tw-border-solid tw-rounded tw-p-3 tw-mb-6"
    >
      <p class="tw-text-rose-600">
        {{ t('playlist.editor.entriesAdd.error') }}
        <template v-if="entryAddError"><br />{{ entryAddError.message }}</template>
      </p>

      <APlaylistEntries
        :entries="nonAddedEntries"
        :can-sort="false"
        :can-edit="false"
        class="tw-mb-3"
      />

      <div class="tw-flex tw-items-center tw-gap-3">
        <button
          type="button"
          class="btn btn-default btn-sm"
          :disabled="isUpdatingPlaylist"
          @click="retryAddEntriesToPlaylist"
        >
          <Loading v-if="isRetrying" class="tw-h-1 tw-mr-1" />
          {{ t('playlist.editor.entriesAdd.retry') }}
        </button>
        <button
          type="button"
          class="btn btn-danger btn-sm"
          :disabled="isUpdatingPlaylist"
          @click="nonAddedEntries = []"
        >
          {{ t('playlist.editor.entriesAdd.discard') }}
        </button>
      </div>
    </div>

    <section
      v-if="uploadedFiles.size > 0"
      class="tw-mb-6"
      :aria-label="t('playlist.editor.upload.pending')"
    >
      <ul class="tw-flex tw-flex-wrap tw-gap-2 tw-p-0 tw-m-0">
        <template v-for="[id, entry] in uploadedFiles" :key="id">
          <AUploadProgress as="li" :name="entry.name" :file-id="id" class="tw-max-w-xs" />
        </template>
      </ul>
    </section>

    <fieldset
      v-if="allowAddEntries"
      ref="dropzoneEl"
      class="tw-rounded tw-bg-gray-50 tw-border-2 tw-border-dashed tw-flex tw-mb-3 tw-p-6"
      :class="{ 'tw-border-teal-600': isOverDropZone, 'tw-border-gray-200': !isOverDropZone }"
    >
      <div class="tw-place-self-center tw-mx-auto tw-flex tw-flex-col tw-gap-2 tw-items-center">
        <icon-system-uicons-file-upload class="tw-text-xl" />
        <p class="tw-mb-0">{{ t('playlist.editor.control.dropFiles') }}</p>
        <p class="tw-text-gray-400 tw-text-sm tw-mb-1.5 tw-leading-none">
          {{ t('playlist.editor.control._or') }}
        </p>
        <div class="tw-flex tw-flex-wrap tw-justify-center tw-items-center tw-gap-3">
          <button type="button" class="btn btn-default" @click="openFileDialog()">
            <icon-iconamoon-file-audio-thin class="tw-flex-none" />
            {{ t('playlist.editor.control.selectFiles') }}
          </button>
          <button type="button" class="btn btn-default" @click="importFileFromURL()">
            <icon-formkit-url class="tw-flex-none" />
            {{ t('playlist.editor.control.importFile') }}
          </button>
        </div>
        <div class="tw-flex tw-flex-wrap tw-justify-center tw-items-center tw-gap-3 tw-mt-1">
          <button type="button" class="btn btn-default" @click="addStreamToPlaylist()">
            <icon-solar-play-stream-bold class="tw-flex-none" />
            {{ t('playlist.editor.control.addStream') }}
          </button>
          <button type="button" class="btn btn-default" @click="addInputToPlaylist">
            <icon-game-icons-jack-plug class="tw-flex-none" />
            {{ t('playlist.editor.control.addInput') }}
          </button>
        </div>
      </div>
    </fieldset>
  </div>

  <GetStreamUrl v-slot="{ resolve, reject }">
    <AStreamURLDialog @save="resolve($event)" @close="reject(undefined)" />
  </GetStreamUrl>

  <GetFileImportUrl v-slot="{ resolve, reject }">
    <AFileUrlDialog @save="resolve($event)" @close="reject(undefined)" />
  </GetFileImportUrl>

  <GetInputUrl v-slot="{ resolve, reject }">
    <AInputUrlDialog @save="resolve($event)" @close="reject(undefined)" />
  </GetInputUrl>
</template>

<script lang="ts" setup>
import { APIResponseError } from '@rokoli/bnb/drf'
import { createTemplatePromise, useDropZone, useFileDialog } from '@vueuse/core'
import { computed, ref, watch } from 'vue'

import { useCopy } from '@/form'
import { useI18n } from '@/i18n'
import { useFilesStore, usePlaylistStore } from '@/stores'
import { File as TankFile, Playlist, PlaylistEntry, Show } from '@/types'
import { getFilenameFromURL, useAsyncFunction } from '@/util'

import AStreamURLDialog from '@/components/playlist/AStreamURLDialog.vue'
import AFileUrlDialog from '@/components/playlist/AFileUrlDialog.vue'
import AInputUrlDialog from '@/components/playlist/AInputUrlDialog.vue'
import Loading from '@/components/generic/Loading.vue'
import AUploadProgress from '@/components/playlist/AUploadProgress.vue'
import SaveIndicator from '@/components/generic/SaveIndicator.vue'
import APlaylistDurationCheck from '@/components/playlist/APlaylistDurationCheck.vue'
import APlaylistEntries from '@/components/playlist/APlaylistEntries.vue'

const props = defineProps<{
  show: Show
  requiredDurationSeconds: number
  playlist: Playlist | null
  useExpertMode?: boolean
}>()
const emit = defineEmits<{
  create: [Playlist]
}>()

const { t } = useI18n()
const fileStore = useFilesStore()
const playlistStore = usePlaylistStore()

const GetStreamUrl = createTemplatePromise<string>()
const GetFileImportUrl = createTemplatePromise<string>()
const GetInputUrl = createTemplatePromise<string>()

const entries = useCopy(() => props.playlist?.entries ?? [], {
  save: () => updatePlaylistEntries(),
})
const allowAddEntries = computed(
  () => props.useExpertMode || !props.playlist || props.playlist.entries.length === 0,
)
const entryAddError = ref<Error>()
const nonAddedEntries = ref<Partial<PlaylistEntry>[]>([])
const isRetrying = ref(false)

const uploadedFiles = ref(new Map<TankFile['id'], { tankFile: TankFile; name: string }>())
const dropzoneEl = ref<HTMLDivElement>()
const { open: openFileDialog, files } = useFileDialog({ accept: 'audio/*', multiple: true })
watch(files, (newFiles) => uploadFiles(Array.from(newFiles ?? [])))
const { isOverDropZone } = useDropZone(dropzoneEl, {
  onDrop: (files) => uploadFiles(files ?? []),
})

async function uploadFiles(files: File[]) {
  const audioFiles = files.filter((f) => f.type.startsWith('audio/'))
  const fileAddResults = await Promise.allSettled(
    audioFiles.map((file) =>
      fileStore.uploadFile(file, props.show, {
        onDone: (tankFile) => uploadedFiles.value.delete(tankFile.id),
        onCreate: (tankFile) => {
          uploadedFiles.value.set(tankFile.id, { tankFile, name: file.name })
        },
      }),
    ),
  )
  const addedFiles = fileAddResults
    .filter((r) => r.status === 'fulfilled')
    .map((r) => (r as PromiseFulfilledResult<TankFile>).value)
  await addFileToPlaylist(...addedFiles)
}

async function importFileFromURL() {
  const fileUrl = await GetFileImportUrl.start()
  const hostname = new URL(fileUrl).hostname
  const tankFile = await fileStore.importFileURL(fileUrl, props.show, {
    onDone: (tankFile) => uploadedFiles.value.delete(tankFile.id),
    onCreate: (tankFile) => {
      uploadedFiles.value.set(tankFile.id, {
        tankFile,
        name: getFilenameFromURL(fileUrl, t('playlistEditor.fileUrlLabel', { hostname })),
      })
    },
  })
  await addFileToPlaylist(tankFile)
}

async function createPlaylist() {
  const playlist = await playlistStore.create({})
  emit('create', playlist)
  return playlist
}

async function addFileToPlaylist(...files: TankFile[]) {
  await updatePlaylistEntries(
    ...files.map((file) => ({ file: { id: file.id, show: props.show.slug } })),
  )
}

async function addStreamToPlaylist() {
  const streamURL = await GetStreamUrl.start()
  await updatePlaylistEntries({ uri: streamURL })
}

async function addInputToPlaylist() {
  const inputUrl = await GetInputUrl.start()
  await updatePlaylistEntries({ uri: inputUrl })
}

const { fn: updatePlaylistEntries, isProcessing: isUpdatingPlaylist } = useAsyncFunction(
  async function (...newEntries: Partial<PlaylistEntry>[]) {
    const playlist = props.playlist ? props.playlist : await createPlaylist()
    try {
      await playlistStore.update(playlist.id, {
        entries: [...entries.value, ...newEntries],
      })
    } catch (e) {
      if (e instanceof APIResponseError) {
        const message = (e.data as Record<string, unknown>)?.error
        if (typeof message === 'string') entryAddError.value = new Error(message)
      }
      nonAddedEntries.value = [...nonAddedEntries.value, ...newEntries]
      throw e
    }
  },
)

async function retryAddEntriesToPlaylist() {
  if (nonAddedEntries.value.length === 0) return
  isRetrying.value = true
  const retryEntries = nonAddedEntries.value
  try {
    await updatePlaylistEntries(...retryEntries)
    nonAddedEntries.value = []
  } catch (e) {
    nonAddedEntries.value = retryEntries
  } finally {
    isRetrying.value = false
  }
}
</script>
