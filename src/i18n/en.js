export default {
  sessionInitialization: {
    OIDC_AUTH: 'Loading user data',
    STEERING_INITIALIZATION: 'Initializing content management session',
    TANK_INITIALIZATION: 'Initializing file storage session',
  },

  loginScreen: {
    intro:
      'AURA is radio automation software for the special needs of community radios. AURA has been developed in several Austrian community radios and it is open source.',
  },

  showManager: {
    title: 'Radio shows',
    generalSettings: 'General settings for show',
  },

  myShows: {
    title: 'My Shows',
    itemsPerPage: 'Shows per page',
    searchLabel: 'Search by name',
    searchPlaceholder: 'Name of the show...',
    showCount: 'Showing %{count} of %{total} shows.',
    showOrder: 'Order of Shows',
    sortShows: 'Order Shows',
  },

  showListGrid: {
    otherShows: 'Other Shows',
  },

  show: {
    singular: 'Show',
    plural: 'Shows',
    fields: {
      id: 'ID',
      name: 'Name',
      slug: 'Slug',
      updatedAt: 'Last modification',
      updatedBy: 'Modified by',
      isActive: 'Active',
      isOwner: 'Administrated by me',
      description: 'Description',
      shortDescription: 'Short description',
      typeId: 'Type',
      fundingCategoryId: 'Funding category',
    },

    editor: {
      deactivation: {
        label: 'Deactivate show',
        confirm: {
          text: 'Are you sure you want to deactivate <strong>%{show}</strong>?',
          prompt: 'Please enter <code>%{challenge}</code> in order to confirm.',
          confirmLabel: 'Deactivate <strong>%{show}</strong>',
        },
      },
      activation: {
        label: 'Activate show',
      },
    },
  },

  showFilter: {
    order: {
      choices: {
        id: { name: 'ID', directions: { asc: '0 - 9', desc: '9 - 0' } },
        slug: { name: 'Name', directions: { asc: 'A - Z', desc: 'Z - A' } },
        is_active: { name: 'Active', directions: { asc: 'No - Yes', desc: 'Yes - No' } },
        is_owner: {
          name: 'Administrated by me',
          directions: { asc: 'No - Yes', desc: 'Yes - No' },
        },
        updated_at: {
          name: 'Last modification',
          directions: { asc: 'Oldest first', desc: 'Newest first' },
        },
        updated_by: { name: 'Modified by', directions: { asc: 'A - Z', desc: 'Z - A' } },
      },
    },
  },

  showEpisode: {
    title: 'Broadcast from %{start}',
  },

  filePlaylistManager: {
    title: 'Media',
    files: 'Files',
    playlists: 'Playlists',
  },

  credits: {
    title: 'Credits',
    intro:
      'AURA is developed under the GNU Affero General Public License v3. Current and former developers are',

    graphicsIcons: 'Graphics & Icons',

    loadingIcon: 'Animated loading icon',
    otherIcons: 'All other icons',

    moreInfo:
      'For more infos visit the <a href="https://gitlab.servus.at/aura/dashboard">Dashboard repository</a>.<br>To get started with AURA, visit <a href="https://aura.radio">aura.radio</a>.<br>All AURA repositories can be found at <a href="https://code.aura.radio">https://code.aura.radio</a>.',
  },

  help: {
    title: 'Help',
    generalDocumentation: 'You can find the general AURA documentation here: ',
  },

  // Layout
  loadMoreItems: 'Load more %{items}',
  loadingData: 'Loading %{items}',
  loading: 'Loading..',
  cancel: 'Cancel',
  delete: 'Delete',
  new: 'New',
  add: 'Add',
  save: 'Save',
  ok: 'OK',
  browse: 'Browse',
  drop: 'Drop file here',
  edit: 'Edit',
  slug: 'Slug',
  unimplemented:
    'By the mighty witchcraftry of the mother of time!\n\nThis feature is not implemented yet.',
  noneSetMasculine: '(none set)',
  noneSetFeminine: '(none set)',
  noneSetNeuter: '(none set)',
  noAssignedShows:
    'You have not been assigned any shows yet.<br>They can be assigned in the <a href="%{adminUrl}">administration interface</a>.',
  by: 'by',

  auth: {
    signOut: 'Sign out',
    signIn: 'Sign in',
    permissionError: 'You are not permitted to view this page',
  },

  construction: {
    label: 'Construction Site',
  },

  navigation: {
    _title: 'Navigation',
    home: 'Home',
    shows: 'Shows',
    show: {
      episodes: 'Broadcasts & Schedules',
      basicData: 'Basic Settings',
    },
    episode: {
      _title: 'Broadcast from %{start}',
      details: 'Description',
    },
    filesPlaylists: 'Media',
    calendar: 'Calendar',
    settings: 'Settings',
    profile: 'Profile',
    help: 'Help',
  },

  footer: {
    tagline: 'All the UI you need to run a community radio',
  },

  // Specific components
  header: {
    showMoreDesktop: 'Hit menu button for more',
    showMoreMobile: 'For more options tap the menu button on the right',
  },

  emissionTable: {
    title: 'Title of emission',
    start: 'Emission start',
    duration: 'Duration',
    playlist: 'Media sources',
    actions: 'Actions',
    missingPlaylistData:
      'Information for this playlist is currently not available. Switch to the show of this emission.',
  },

  fileManager: {
    noFilesAvailable: 'There are no files for this show yet',
    upload: 'Add or upload file',
    error: 'Error',

    importAborted: 'The import was aborted',
    importLogTooltip: 'Show import log',
  },

  fileAdder: {
    title: 'Add or upload file',

    link: 'Link',
    linkPlaceholder: 'Enter a HTTP(S) URL',
    download: 'Download from remote source instead of uploading file',

    editMeta: 'Edit metadata',
  },

  importLog: {
    title: 'File import log',
    availableLogs: 'Available logs',

    fetchError: 'Error: Could not fetch import log from Tank. Check browser console for more info',
    fetchLog: 'Fetch log',
    normalizeLog: 'Normalization Log',
  },

  playlistSelector: {
    title: 'Assign playlist',

    noPlaylistsAvailable: 'No playlists available. Use the buttons below to create some!',
    currentPlaylistLabel: 'Currently selected playlist ID',

    upload: 'Upload',
    uploadAudio: 'Upload audio file',
    goToFiles: "Switch to 'Files & Playlists'",

    mismatchedLength: 'The playlist and the timeslot have different durations',
    mismatchedLengthConfirmation:
      'You are about to assign a playlist with a mismatched duration. Are you sure?',
  },

  playlistManager: {
    noPlaylistsAvailable: 'There are no playlists for this show yet',
    create: 'Create playlist',
  },

  playlistEditor: {
    titleExisting: 'Editing playlist %{id} for show "%{show}"',
    titleNew: 'Add new playlist for show "%{show}"',

    noEntriesAvailable: 'There are no entries yet. Go ahead and add some!',
    saved: 'The playlist has been saved',

    unknownDuration: '(Unkown)',
    tooManyUnknowns:
      'There are two or more entries that have an unknown duration. Please fix this by clicking on the duration field in the table and entering one manually. A playlist may only contains one entry with an unknown duration.',

    types: {
      file: 'File',
      lineIn: 'Line-in',
      stream: 'Stream',
      other: 'Other',
    },

    addStream: 'Add stream to playlist',
    invalidDurationFormat: 'Please enter a duration in format mm:ss or hh:mm:ss',
  },

  input: {
    unknown: 'Unknown Input',
  },

  file: {
    url: 'URL',
    unnamed: 'Unnamed file',
    duration: 'Duration',
    durationUnknown: 'Unknown duration',
    durationPlaceholder: 'e.g. 2m6s',
    uploadProgress: 'Progress of the file upload',
    metadata: {
      _title: 'Metadata',
      album: 'Album',
      artist: 'Artist',
      title: 'Title',
      fromAlbum: 'From the album <em>%{album}</em>.',
      byArtist: 'By <em>%{artist}</em>.',
      byArtistFromAlbum: 'By <em>%{artist}</em> from their album <em>%{album}</em>.',
    },
    importLog: {
      title: 'File import logs',
      showImportLog: 'Show import logs',
      start: '# %{title} started at %{time}',
      type: { fetch: 'Fetch log', normalize: 'Normalization log' },
    },
  },

  playlist: {
    duration: 'Duration',
    editor: {
      title: 'Media sources',
      expertMode: 'Expert mode',
      isSaving: 'Media sources are being saved',
      editPlaylistEntry: 'Edit media sources',
      deletePlaylistEntry: 'Delete media source',
      entriesAdd: {
        retry: 'Try again',
        discard: 'Discard',
        error: 'Some of the media sources could not be added.',
      },
      upload: {
        pending: 'Pending uploads',
        step: {
          fetching: 'Importing',
          normalizing: 'Normalizing',
          unknown: 'Processing',
        },
      },
      control: {
        _or: '- or -',
        dropFiles: 'Drag files on this area',
        selectFiles: 'Select local file',
        importFile: 'Import file from URL',
        addStream: 'Add stream',
        addInput: 'Add input',
      },
      importFileDialog: {
        title: 'Import file from URL',
        saveLabel: 'Add file',
      },
      addInputDialog: {
        title: 'Add input as media source',
        saveLabel: 'Add input',
      },
      addStreamDialog: {
        title: 'Add stream as media source',
        saveLabel: 'Add stream',
      },
    },
    state: {
      ok: { title: 'Perfect' },
      missing: { title: 'Missing' },
      indeterminate: {
        title: 'Unknown duration',
        description: `At least one of the media sources does not have a fixed duration.
          It is therefore not possible to check whether your sources exceed or fall short of the broadcast duration.
          The media you have provided should have a total length of <strong>%{totalTime}</strong>.
          The current sources are deviating from this by <strong>%{offset}</strong>.`,
      },
      tooShort: {
        title: 'Too short',
        description: `The media sources you have provided fall short of the duration of the broadcast by <strong>%{offset}</strong>.
          <br>Please add further content to your episode.`,
      },
      tooLong: {
        title: 'Too long',
        description: `The media sources you have provided exceed the duration of the broadcast by <strong>%{offset}</strong>.
          <br>Please shorten your episode content accordingly.`,
      },
    },
  },

  playlistTable: {
    // Our translation framework automatically picks singular/plural
    items: '%{smart_count} item |||| %{smart_count} items',
    assign: 'Assign',
    unset: 'Unset',

    index: 'Nr',
    description: 'Description',
    entries: 'Entries',
    duration: 'Duration',
    lastEdit: 'Last edited',
    type: 'Type',
    source: 'Source',
    actions: 'Actions',
  },

  showSelector: {
    keyboardShortcut: 'Alt + K',
    selectShowMany: 'Select show:',
    selectShow: 'Select a radio show',
    noData: 'No shows have been created yet.',
    noDataMatch: 'There are no shows that match your search criteria.',
    showState: {
      label: 'Show status',
      active: 'Active',
      inactive: 'Inactive',
    },
  },

  showSchedules: {
    title: 'Schedules',
    times: 'Airtime',
    rhythm: 'Rhythm',
    firstBroadcast: 'First airing',
    lastBroadcast: 'Last airing',
    noSchedulesAvailable: 'There are currently no schedules for this show',
    showAll: 'Show All',
    hide: 'Hide',
    scheduleDescription:
      '%{rhythm} beginning %{startDate} from %{startTime} to %{endTime} respectively.',
    scheduleDescriptionFinite:
      '%{rhythm} beginning %{startDate} through %{endDate} from %{startTime} to %{endTime} respectively.',
  },

  showTimeslots: {
    title: 'Broadcasts',
    noTimeslotsScheduled: 'No broadcasts are scheduled within the given timeframe.',
    filter: 'Filter',

    editDescription: 'Edit description',
    editPlaylist: 'Edit playlist',
    openPlayer: 'Open player',
    downloadRecording: 'Download recording',
    uploadToCba: 'Upload recording to CBA',

    numberOfSlots: 'Number of slots',
    from: 'From',
    to: 'To (exclusive)',
  },

  showMeta: {
    email: 'Email',
    emailPlaceholder: 'Enter a contact address for this show',
    editEmail: 'Edit email',
    invalidEmail: 'Please enter a valid email',

    showName: 'Show name',
    showNamePlaceholder: 'Enter the name of the show',
    editShowName: 'Edit show name',

    description: 'Description',
    descriptionPlaceholder: 'Enter a description',
    editDescription: 'Edit description',

    shortDescription: 'Short description',
    shortDescriptionPlaceholder: 'Enter a short description of this show',
    editShortDescription: 'Edit short description',

    internalNote: 'Internal note',

    website: 'Website',
    websitePlaceholder: 'Enter a website for this show',
    editWebsite: 'Edit website',
    invalidWebsite: 'Please enter a valid URL',

    type: 'Show type',
    editType: 'Edit show type',

    fundingCategory: 'Funding category',
    fundingCategoryRtr: '(e.g. for RTR)',
    editFundingCategory: 'Edit funding category',

    predecessor: 'Predecessor',
    noPredecessor: 'This show has no predecessor',
    editPredecessor: 'Edit predecessor',
    noPredecessorName: 'The name of the predecessor is unavailable',

    cbaSeriesId: 'CBA Series ID',
    cbaSeriesIdPlaceholder: 'Enter a CBA Series ID for this show',
    editCbaSeriesId: 'Edit CBA Series ID',
    invalidCbaSeriesId: 'Please enter a valid integer as series ID',

    defaultPlaylist: 'Default show playlist ID',
    editDefaultPlaylist: 'Edit default show playlist ID',

    // Meta arrays
    categories: 'Categories',
    categoriesLabel: 'Select one or more categories',
    editCategories: 'Edit categories',

    topics: 'Topics',
    topicsLabel: 'Select one or more topics',
    editTopics: 'Edit topics',

    genres: 'Genres',
    genresLabel: 'Select one or more genres',
    editGenres: 'Edit genres',

    languages: 'Languages',
    languagesLabel: 'Select one or more languages',
    editLanguages: 'Edit languages',

    hosts: 'Hosts',
    hostsLabel: 'Select one or more hosts',
    editHosts: 'Edit hosts',

    owners: 'Owners',
    editOwners: 'Edit owners',

    accessAlreadyGiven: 'This user already has access for this show',
    usersWithAccess: 'Users with access to <b>%{show}</b>',
    addNewUsers: 'You can add new users using the table below',

    name: 'Name',
    username: 'Username',
    options: 'Options',

    logo: 'Logo',
    image: 'Image',

    multiselect: '<b>Hint:</b> use <code>CTRL+click</code> for multiple selection',
  },

  user: {
    username: 'Username',
    name: 'Name',
    email: 'Email',
  },

  noteEditor: {
    _title: 'Broadcast description',
    title: 'Title',
    titlePlaceholder: 'Enter a title',
    summary: 'Summary',
    summaryPlaceholder: 'Enter a summary',
    content: 'Content',
    contentPlaceholder: 'Describe the content of the show',
    image: 'Image',
    tags: 'Tags',
    contributors: 'Contributors',
    languages: 'Languages',
    topics: 'Topics',
  },

  error: {
    server: {
      unknown: 'An unknown error server error occurred.',
      null: 'This field cannot be empty.',
      blank: 'This field cannot be empty.',
      unique: 'The value of this field must be unique.',
    },
  },

  imagePicker: {
    title: 'Pick an Image',
    browseImages: 'Browse Images',
    editCurrentImage: 'Edit the current image',
    saveChanges: 'Save changes',
    useImage: 'Use this image',
    chooseAnImage: 'Choose image',
    uploadImage: 'Upload new image',
    abort: 'Abort',
    reset: 'Reset Image',
    error: {
      default: 'Could not upload image.',
      tooLarge: 'The image you’ve selected is too large in filesize.',
    },
  },

  image: {
    altText: 'Alternative Text (for screen readers)',
    license: 'License',
    credits: 'Credits / Author',
    isUseExplicitlyGrantedByAuthor: 'Use was explicitly granted by author',
  },

  license: {
    validation: {
      requiresExpressPermissionForPublication:
        'The license requires explicit consent for publication on the part of the author.',
      requiresCredits:
        'The license requires attribution of the author. Please indicate their name.',
    },
  },

  saveIndicator: {
    label: 'Saving',
  },

  showCreator: {
    title: 'Create new show',
    missingShowTypes:
      'No show types have been created yet. You can add them in the <a href="%{adminUrl}/program/type/add/" target="_blank">administration interface</a>.',
    missingShowFundingCategories:
      'No funding categories have been created yet. You can add them in the <a href="%{adminUrl}/program/fundingcategory/add/" target="_blank">administration interface</a>.',
  },

  conflictResolution: {
    title: 'Conflict resolution for new schedule',
    titleModal: 'Resolve a timeslot conflict',

    titleNoConflict: 'No conflict to resolve',
    noConflict: 'This timeslot does not have a conflict. Great!',

    conflictingSlot: 'New Timeslot',
    projectedSlot:
      'The new timeslot runs on <b>%{firstDate}</b> from <b>%{startTime}</b> to <b>%{endTime}</b>',
    conflictsWith: 'It conflicts with the following timeslots',

    newSchedule: 'from <b>%{firstDate}, %{startTime}</b> to <b>%{endTime}</b>',
    recurringSchedule: 'This schedule repeats <b>%{rrule}</b> until <b>%{lastDate}</b>',
    leftToResolve:
      '%{smart_count} conflicts left to resolve |||| %{smart_count} conflict left to resolve',
    noneLeftToResolve: 'No more conflicts to resolve!',
    applySolution: 'Apply solution',

    from: 'From',
    to: 'To',
    showName: 'Show name',

    whichStrategy: 'Which solution should be applied?',
    strategies: {
      ours: 'Insert new timeslot, delete old one',
      theirs: 'Delete new timeslot, keep old one',
      theirsStart: 'Insert new timeslot after old timeslot',
      theirsEnd: 'Insert new timeslot before old timeslot',
      theirsBoth: 'Insert new timeslot with interruption by old timeslot',
      oursStart: 'ours-start (TODO: Better description)',
      oursEnd: 'ours-end (TODO: Better description)',
      oursBoth: 'ours-both (TODO: Better description)',
    },
  },

  paginator: {
    gotoFirst: 'Go to first page',
    gotoPrev: 'Go to previous page',
    goto: 'Go to page %{page}',
    gotoNext: 'Go to next page',
    gotoLast: 'Go to last page',
    range: 'Showing %{start} to %{end} of %{count} entries.',
  },

  scheduleEditor: {
    titleCreate: 'Create new schedule for "%{show}"',
    titleEdit: 'Edit schedule for "%{show}"',

    timeslotRuns:
      'This timeslot runs on <b>%{firstDate}</b> from <b>%{startTime}</b> to <b>%{endTime}</b>',
    recurringSchedule: 'This schedule repeats from <b>%{rrule}</b> to <b>%{lastDate}</b>',

    singleEmission: 'This timeslot is a one-time emission.',
    coexistingTimeslot:
      'But due to a conflict resolution there is another timeslot on <b>%{firstDate}</b> from <b>%{startTime}</b> to <b>%{endTime}</b>',

    addFallbackTitle: 'Add fallback for schedule',
    currentlySelectedPlaylist: 'Currently selected playlist',
    assignPlaylist: 'Assign playlist',

    start: 'Start',
    end: 'End',
    projectedDuration: '<b>Projected duration:</b> %{duration}',

    pastEventWarning:
      'Past events will be ignored. Start date was set to today!<br>Try again or change the start date to something in the future.',
    from: 'From',
    to: 'To',
    fromDate: 'Starting',
    toDate: 'Until <span class="tw-font-normal tw-text-gray-500">(Date)</span>',

    addRepetition: 'Add repetition schedule',
    whenToRepeat: 'When to repeat?',
    useSameTime: 'Use same start and end time',
    repeatAt: 'Repeat at',
    addNoOfDays: 'How many days later?',
    onlyBusinessDays: 'Only count business days',

    delete: {
      delete: 'Delete',
      timeslotsTitle: 'Deleting timeslots',
      both: 'Delete both',
      scheduleTimeslots: 'Delete schedule and all timeslots',
      timeslot: 'Delete timeslot',
      allTimeslots: 'Delete all future timeslots',
    },

    repetition: {
      followingDay: 'On the following day',
      followingBusinessDay: 'On the following business day',
      numberOfDaysLater: 'A number of days later',
    },
  },

  editingMetadata: {
    timeSpecific: 'on %{date} at %{time}',
    lastModified: 'Last modified %{time} by %{author}.',
    created: 'Created %{time} by %{author}.',
  },

  calendar: {
    today: 'Today',
    empty: 'No playlist',
    fallback: 'Fallback playlist',
    repetition: 'Repetition',
    view: {
      day: 'Day view',
      week: 'Week view',
    },
    switchShow: 'Switch to “%{show}” show',
    editTimeslot: 'Sendung bearbeiten',
    intermission: 'Program intermission',
    mismatchedLength: 'wrong duration',
    playing: 'Currently playing',
  },

  // Etc
  metadata: {
    artist: 'Artist',
    album: 'Album',
    title: 'Title',
    source: 'Source',
    size: 'File size',
  },

  // Etc
  rrule: {
    rrule: 'Repetition rule',

    rule: {
      1: 'once',
      2: 'daily',
      3: 'workdays',
      4: 'weekly',
      5: 'every two weeks',
      6: 'every four weeks',
      7: 'even calendar weeks',
      8: 'uneven calendar weeks',
      9: 'Every first week of the month',
      10: 'Every second week of the month',
      11: 'Every third week of the month',
      12: 'Every fourth week of the month',
      13: 'Every fifth week of the month',
    },
  },

  steeringErrorCodes: {
    'no-start-after-end': 'The start date cannot be after the end date.',
    'no-same-day-start-and-end': 'The start and end date cannot be on the same day.',
    'one-solution-per-conflict':
      'Exactly one solution is required per conflict. You either submitted no or more than one solution.',
  },
}
