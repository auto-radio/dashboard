export default {
  sessionInitialization: {
    OIDC_AUTH: 'Lade Nutzer:innendaten',
    STEERING_INITIALIZATION: 'Initialisiere Inhaltsverwaltung',
    TANK_INITIALIZATION: 'Initialisiere Dateiverwaltung',
  },

  loginScreen: {
    intro:
      'AURA ist Radioautomationssuite die speziell auf die Bedürfnisse von Freien Radios zugeschnitten ist. AURA wurde in der Gemeinschaft mehrerer österreichischer Community-Radios entwickelt und ist quelloffen.',
  },

  showManager: {
    title: 'Sendereihen',
    generalSettings: 'Allgemeine Einstellungen der Sendereihe',
  },

  myShows: {
    title: 'Meine Sendereihen',
    itemsPerPage: 'Sendereihen pro Seite',
    searchLabel: 'Suche nach Namen',
    searchPlaceholder: 'Name der Sendereihe...',
    showCount: 'Zeige %{count} von %{total} Sendereihen.',
    showOrder: 'Sortierung der Sendereihen',
    sortShows: 'Sendereihen sortieren',
  },

  showListGrid: {
    otherShows: 'Weitere Sendereihen',
  },

  show: {
    singular: 'Sendereihe',
    plural: 'Sendereihen',
    fields: {
      id: 'ID',
      name: 'Name',
      slug: 'Kürzel',
      updatedAt: 'Letzte Aktualisierung',
      updatedBy: 'Aktualisiert von',
      isActive: 'Aktiv',
      isOwner: 'Verwaltet von mir',
      description: 'Beschreibung',
      shortDescription: 'Kurzbeschreibung',
      typeId: 'Art',
      fundingCategoryId: 'Förderkategorie',
    },
    editor: {
      deactivation: {
        label: 'Sendereihe deaktivieren',
        confirm: {
          text: 'Bist du sicher, dass du <strong>%{show}</strong> deaktivieren willst?',
          prompt: 'Bitte gib <code>%{challenge}</code> als Bestätigung ein.',
          confirmLabel: 'Deaktiviere <strong>%{show}</strong>',
        },
      },
      activation: {
        label: 'Sendereihe aktivieren',
      },
    },
  },

  showFilter: {
    order: {
      choices: {
        id: { name: 'ID', directions: { asc: '0 - 9', desc: '9 - 0' } },
        slug: { name: 'Name', directions: { asc: 'A - Z', desc: 'Z - A' } },
        is_active: { name: 'Aktiv', directions: { asc: 'Nein - Ja', desc: 'Ja - Nein' } },
        is_owner: {
          name: 'Verwaltet von mir',
          directions: { asc: 'Nein - Ja', desc: 'Ja - Nein' },
        },
        updated_at: {
          name: 'Letzte Aktualisierung',
          directions: { asc: 'Älteste zuerst', desc: 'Neueste zuerst' },
        },
        updated_by: { name: 'Aktualisiert von', directions: { asc: 'A - Z', desc: 'Z - A' } },
      },
    },
  },

  showEpisode: {
    title: 'Sendung vom %{start}',
  },

  filePlaylistManager: {
    title: 'Medien',
    files: 'Dateien',
    playlists: 'Playlists',
  },

  credits: {
    title: 'Credits',
    intro:
      'AURA wird unter der GNU Affero General Public License v3 entwickelt. Aktive und vergangene Entwickler sind:',
    graphicsIcons: 'Grafiken & Icons',

    loadingIcon: 'Animiertes Lade-Icon',
    otherIcons: 'Alle weiteren Icons',

    moreInfo:
      'Mehr Informationen gibt es im <a href="https://gitlab.servus.at/aura/dashboard">Dashboard-Repository</a>.<br>Um eine Gesamtübersicht über AURA zu erhalten, besuche <a href="https://aura.radio">aura.radio</a>.<br>Alle AURA-Repositories können auf <a href="https://code.aura.radio">https://code.aura.radio</a> eingesehen werden.',
  },

  help: {
    title: 'Hilfe',
    generalDocumentation: 'Die generelle AURA-Dokumentation findest Du hier: ',
  },

  // Layout
  loadMoreItems: 'Mehr %{items} laden',
  loadingData: 'Lade %{items}',
  loading: 'Lädt..',
  cancel: 'Abbrechen',
  delete: 'Löschen',
  add: 'Hinzufügen',
  save: 'Speichern',
  ok: 'OK',
  browse: 'Auswählen',
  drop: 'Datei hier ablegen',
  edit: 'Bearbeiten',
  slug: 'Slug',
  unimplemented: 'Potzblitz!\n\nDieses Feature ist noch nicht implementiert!',
  noneSetMasculine: '(keiner gesetzt)',
  noneSetFeminine: '(keine gesetzt)',
  noneSetNeuter: '(keines gesetzt)',
  noAssignedShows:
    'Dir sind noch keine Sendereihen zugewiesen.<br>Diese können im <a href="%{adminUrl}">Administrationsbereich</a> zugewiesen werden.',
  by: 'von',

  auth: {
    signOut: 'Abmelden',
    signIn: 'Anmelden',
    permissionError: 'Du bist nicht berechtigt diese Seite zu sehen',
  },

  construction: {
    label: 'Baustelle',
  },

  navigation: {
    _title: 'Navigation',
    home: 'Home',
    shows: 'Sendereihen',
    show: {
      episodes: 'Ausstrahlungen & Schema',
      basicData: 'Allgemeine Einstellungen',
    },
    episode: {
      _title: 'Sendung vom %{start}',
      details: 'Beschreibung',
    },
    filesPlaylists: 'Medien',
    calendar: 'Kalender',
    settings: 'Einstellungen',
    profile: 'Profil',
    help: 'Hilfe',
  },

  footer: {
    tagline: 'Alles was Du für ein Freies Radio brauchst',
  },

  // Specific components
  header: {
    showMoreDesktop: 'Klicke den Menü-Knopf für mehr',
    showMoreMobile: 'Für mehr Optionen, klicke auf den Menü-Knopf rechts',
  },

  emissionTable: {
    title: 'Ausstrahlungstitel',
    start: 'Start der Ausstrahlung',
    duration: 'Dauer',
    playlist: 'Medienquellen',
    actions: 'Aktionen',
    missingPlaylistData:
      'Informationen zur Playlist stehen gerade nicht zur Verfügung. Wechsle zur Sendereihe dieser Ausstrahlung.',
  },

  fileManager: {
    noFilesAvailable: 'Für diese Sendereihe gibt es noch keine Dateien',
    upload: 'Datei hochladen oder hinzufügen',
    error: 'Fehler',

    importAborted: 'Der Import wurde abgebrochen',
    importLogTooltip: 'Import-Log anzeigen',
  },

  fileAdder: {
    title: 'Datei hochladen oder hinzufügen',

    link: 'Link',
    linkPlaceholder: 'Gib eine HTTP(S)-URL ein',
    download: 'Von Drittquelle herunterladen, statt Datei hochzuladen',

    editMeta: 'Metadaten bearbeiten',
  },

  importLog: {
    title: 'Log zum Dateiimport',
    availableLogs: 'Verfügbare Logs',

    fetchError:
      'Fehler: Konnte den Import-Log nicht von Tank laden. Schau in die Browserkonsole für mehr Info',
    fetchLog: 'Fetch-Log',
    normalizeLog: 'Normalisierungs-Log',
  },

  playlistSelector: {
    title: 'Playlist zuweisen',
    defaultTitle: 'Standardplaylist zuweisen',

    noPlaylistsAvailable:
      'Keine Playlists verfügbar. Verwende die Buttons unten um welche zu erstellen!',
    currentPlaylistLabel: 'Momentan gewählte Playlist-ID',

    upload: 'Hochladen',
    uploadAudio: 'Audio-Datei hochladen',
    goToFiles: "Zu 'Dateien & Playlists' gehen",

    mismatchedLength: 'Die Playlist und der Sendeplatz haben eine unterschiedliche Dauer.',
    mismatchedLengthConfirmation:
      'Du bist kurz davor eine Playlist mit unpassender Länge zuzuweisen. Bist du sicher?',
  },

  playlistManager: {
    noPlaylistsAvailable: 'Für diese Sendereihe gibt es noch keine Playlists',
    create: 'Playlist erstellen',
  },

  playlistEditor: {
    titleExisting: 'Playlist %{id} für Sendereihe "%{show}" bearbeiten',
    titleNew: 'Neue Playlist für Sendereihe "%{show}" hinzufügen',

    noEntriesAvailable: 'Keine Einträge vorhanden. Füge doch welche hinzu!',
    saved: 'Die Playlist wurde gespeichert',

    unknownDuration: '(Unbekannt)',
    tooManyUnknowns:
      'Es gibt 2 oder mehr Einträge die eine unbekannte Dauer haben. Bitte korrigiere das, indem du auf die Dauer in der Tabelle klickst und manuell eine einträgst. Es darf maximal ein Eintrag mit unbekannter Dauer vorhanden sein.',

    types: {
      file: 'Datei',
      lineIn: 'Line-in',
      stream: 'Stream',
      other: 'Andere',
    },

    addStream: 'Stream zur Playlist hinzufügen',
    invalidDurationFormat: 'Bitte gib eine Dauer im Format mm:ss oder hh:mm:ss an.',
  },

  input: {
    unknown: 'Unbekannter Eingang',
  },

  file: {
    url: 'URL',
    unnamed: 'Unbenannte Datei',
    duration: 'Dauer',
    durationUnknown: 'Unbekannte Dauer',
    durationPlaceholder: 'z.B. 2m6s',
    uploadProgress: 'Fortschritt des Dateiuploads',
    metadata: {
      _title: 'Metadaten',
      album: 'Album',
      artist: 'Künstler:in',
      title: 'Titel',
      fromAlbum: 'Vom Album <em>%{album}</em>.',
      byArtist: 'Von <em>%{artist}</em>.',
      byArtistFromAlbum: 'Von <em>%{artist}</em> aus dem Album <em>%{album}</em>.',
    },
    importLog: {
      title: 'Dateiimportprotokolle',
      showImportLog: 'Zeige Importprotokolle',
      start: '# %{title} beginnt am %{time}',
      type: { fetch: 'Datenabrufprotokoll', normalize: 'Normalisierungsprotokoll' },
    },
  },

  playlist: {
    duration: 'Dauer',
    editor: {
      title: 'Medienquellen',
      expertMode: 'Expert:innenmodus',
      isSaving: 'Medienquellen werden gespeichert',
      editPlaylistEntry: 'Medienquelle bearbeiten',
      deletePlaylistEntry: 'Medienquelle löschen',
      entriesAdd: {
        retry: 'Erneut versuchen',
        discard: 'Verwerfen',
        error: 'Einige Einträge konnten nicht hinzugefügt werden.',
      },
      upload: {
        pending: 'Laufende Uploads',
        step: {
          fetching: 'Importiere',
          normalizing: 'Normalisiere',
          unknown: 'Verarbeite',
        },
      },
      control: {
        _or: '- oder -',
        dropFiles: 'Ziehe Dateien auf diese Fläche',
        selectFiles: 'Lokale Datei auswählen',
        importFile: 'Datei von URL importieren',
        addStream: 'Stream hinzufügen',
        addInput: 'Eingang hinzufügen',
      },
      importFileDialog: {
        title: 'Datei von URL importieren',
        saveLabel: 'Datei hinzufügen',
      },
      addInputDialog: {
        title: 'Eingang als Medienquelle hinzufügen',
        saveLabel: 'Eingang hinzufügen',
      },
      addStreamDialog: {
        title: 'Stream als Medienquelle hinzufügen',
        saveLabel: 'Stream hinzufügen',
      },
    },
    state: {
      ok: { title: 'Perfekt' },
      missing: { title: 'Fehlt' },
      indeterminate: {
        title: 'Unbekannte Länge',
        description: `Mindestens eine hinterlegte Quelle hat keine definierte Länge.
          Es kann daher nicht geprüft werden, ob deine Quellen die Dauer der Ausstrahlung über- oder unterschreiten.
          Die von dir hinterlegten Medienquellen, sollten eine Gesamtlänge von <strong>%{totalTime}</strong> erreichen.
          Diese weichen derzeit um <strong>%{offset}</strong> davon ab.`,
      },
      tooShort: {
        title: 'Unterlänge',
        description: `Die von dir hinterlegten Medienquellen unterschreiten die Dauer der Ausstrahlung um <strong>%{offset}</strong>.
          <br>Bitte ergänze weitere Inhalte für deine Sendung.`,
      },
      tooLong: {
        title: 'Überlänge',
        description: `Die von dir hinterlegten Medienquellen überschreiten die Dauer der Ausstrahlung um <strong>%{offset}</strong>.
          <br>Bitte kürze deine Sendungsinhalt entsprechend.`,
      },
    },
  },

  playlistTable: {
    // Our translation framework automatically picks singular/plural
    items: '%{smart_count} Eintrag |||| %{smart_count} Einträge',
    assign: 'Zuweisen',
    unset: 'Zuweisung aufheben',

    index: 'Nr',
    description: 'Beschreibung',
    entries: 'Einträge',
    duration: 'Dauer',
    lastEdit: 'Bearbeitet am',
    type: 'Typ',
    source: 'Quelle',
    actions: 'Aktionen',
  },

  showSelector: {
    keyboardShortcut: 'Alt + K',
    selectShowMany: 'Sendereihe wählen',
    selectShow: 'Sendereihe auswählen',
    noData: 'Es wurden bisher keine Sendereihen angelegt.',
    noDataMatch: 'Es gibt keine Sendereihen, die deinen Suchkriterien entsprechen.',
    showState: {
      label: 'Status der Sendereihe',
      active: 'Aktiv',
      inactive: 'Inaktiv',
    },
  },

  showSchedules: {
    title: 'Ausstrahlungsschema',
    times: 'Sendezeiten',
    rhythm: 'Rhythmus',
    firstBroadcast: 'Erste Ausstrahlung',
    lastBroadcast: 'Letzte Ausstrahlung',
    noSchedulesAvailable: 'Es gibt für diese Sendereihe noch kein Ausstrahlungsschema',
    showAll: 'Alles anzeigen',
    hide: 'Ausblenden',
    scheduleDescription:
      '%{rhythm} ab %{startDate} jeweils von %{startTime} Uhr bis %{endTime} Uhr.',
    scheduleDescriptionFinite:
      '%{rhythm} ab %{startDate} bis zum %{endDate} jeweils von %{startTime} Uhr bis %{endTime} Uhr.',
  },

  showTimeslots: {
    title: 'Ausstrahlungen',
    noTimeslotsScheduled: 'Im gewählten Zeitraum sind keine Ausstrahlungen geplant.',
    filter: 'Filtern',

    editDescription: 'Beschreibung bearbeiten',
    editPlaylist: 'Playlist bearbeiten',
    openPlayer: 'Player öffnen',
    downloadRecording: 'Aufnahme herunterladen',
    uploadToCba: 'Aufnahme zum CBA hochladen',

    numberOfSlots: 'Anzahl an Ausstrahlungen',
    from: 'Von',
    to: 'Bis (exklusive)',
  },

  showMeta: {
    email: 'E-Mail',
    emailPlaceholder: 'Gib eine Kontaktadresse für die Sendereihe an',
    editEmail: 'E-Mail-Adresse bearbeiten',
    invalidEmail: 'Bitte gib eine gültige E-Mail ein',

    showName: 'Sendungsname',
    showNamePlaceholder: 'Gib den Namen der Sendereihe ein',
    editShowName: 'Sendungsname bearbeiten',

    description: 'Beschreibung',
    descriptionPlaceholder: 'Gib eine Beschreibung ein',
    editDescription: 'Beschreibung bearbeiten',

    shortDescription: 'Kurzbeschreibung',
    shortDescriptionPlaceholder: 'Gib eine Kurzbeschreibung der Sendereihe ein',
    editShortDescription: 'Kurzbeschreibung bearbeiten',

    internalNote: 'Interne Notiz',

    website: 'Webseite',
    websitePlaceholder: 'Gib eine Webseite für die Sendereihe an',
    editWebsite: 'Webseite bearbeiten',
    invalidWebsite: 'Bitte gib eine gültige URL ein',

    type: 'Art der Sendereihe',
    editType: 'Art der Sendereihe bearbeiten',

    fundingCategory: 'Förderkategorie',
    fundingCategoryRtr: '(z.B. für RTR)',
    editFundingCategory: 'Förderkategorie bearbeiten',

    predecessor: 'Vorgänger',
    noPredecessor: 'Diese Sendereihe hat keinen Vorgänger',
    editPredecessor: 'Vorgänger bearbeiten',
    noPredecessorName: 'Der Name des Vorgängers ist nicht verfügbar',

    cbaSeriesId: 'CBA Sendungs-ID',
    cbaSeriesIdPlaceholder: 'Gib eine CBA Sendungs-ID für die Sendereihe an',
    editCbaSeriesId: 'CBA Sendungs-ID bearbeiten',
    invalidCbaSeriesId: 'Bitte gib eine Ganzzahl als Sendungs-ID an.',

    defaultPlaylist: 'Standardplaylist für Sendereihe',
    editDefaultPlaylist: 'Standardplaylist bearbeiten',

    // Meta arrays
    categories: 'Kategorien',
    categoriesLabel: 'Wähle eine oder mehrere Kategorien aus',
    editCategories: 'Kategorien bearbeiten',

    topics: 'Themen',
    topicsLabel: 'Wähle ein oder mehrere Themen aus',
    editTopics: 'Themen bearbeiten',

    genres: 'Genres',
    genresLabel: 'Wähle ein oder mehrere Genres aus',
    editGenres: 'Genres bearbeiten',

    languages: 'Sprachen',
    languagesLabel: 'Wähle eine oder mehrere Sprachen aus',
    editLanguages: 'Sprachen bearbeiten',

    hosts: 'Moderator*innen',
    hostsLabel: 'Wähle eine oder mehrere Moderator*innen aus',
    editHosts: 'Moderator*innen bearbeiten',

    owners: 'Sendungsmacher*innen',
    editOwners: 'Sendungsmacher*innen bearbeiten',

    accessAlreadyGiven: 'Dieser Benutzer hat bereits Zugriff auf diese Sendung',
    usersWithAccess: 'Benutzer mit Zugriff auf <b>%{show}</b>',
    addNewUsers: 'Du kannst aus der Tabelle unten neue Benutzer hinzufügen',

    name: 'Name',
    username: 'Benutzername',
    options: 'Optionen',

    logo: 'Logo',
    editLogo: 'Logo bearbeiten',
    currentLogo: 'Aktuelles Logo',
    chooseLogo: 'Neues Logo auswählen',

    image: 'Bild',
    editImage: 'Bild bearbeiten',
    currentImage: 'Aktuelles Bild',
    chooseImage: 'Neues Bild auswählen',

    multiselect:
      '<b>Hinweis:</b> verwende <code>Strg + Klick</code> um mehrere Kategorien zu wählen',
  },

  user: {
    username: 'Benutzername',
    name: 'Name',
    email: 'E-Mail',
  },

  noteEditor: {
    _title: 'Sendungsbeschreibung',
    title: 'Titel',
    titlePlaceholder: 'Gib einen Titel ein',
    summary: 'Zusammenfassung',
    summaryPlaceholder: 'Gib eine Zusammenfassung ein',
    content: 'Inhalt',
    contentPlaceholder: 'Beschreibe den Inhalt der Sendung',
    image: 'Bild',
    tags: 'Schlagwörter',
    contributors: 'Mitwirkende',
    languages: 'Sprachen',
    topics: 'Themen',
  },

  error: {
    server: {
      unknown: 'Ein unbekannter Serverfehler ist aufgetreten.',
      null: 'Dieses Feld darf nicht leer sein.',
      blank: 'Dieses Feld darf nicht leer sein.',
      unique: 'Der Wert dieses Felds muss einzigartig sein.',
    },
  },

  imagePicker: {
    title: 'Suche ein Bild aus',
    browseImages: 'Bilder durchstöbern',
    editCurrentImage: 'Aktuelles Bild bearbeiten',
    saveChanges: 'Änderungen speichern',
    useImage: 'Dieses Bild wählen',
    chooseAnImage: 'Bild auswählen',
    uploadImage: 'Neues Bild hochladen',
    abort: 'Abbrechen',
    reset: 'Bild zurücksetzen',
    error: {
      default: 'Das Bild konnte nicht hochgeladen werden.',
      tooLarge: 'Die Dateigröße des Bilds, das von dir ausgewählt wurde, ist zu groß.',
    },
  },

  image: {
    altText: 'Alternativtext (für Screenreader)',
    license: 'Lizenz',
    credits: 'Bildnachweis / Urheber:in',
    isUseExplicitlyGrantedByAuthor: 'Autor:in erlaubt Veröffentlichung',
  },

  license: {
    validation: {
      requiresExpressPermissionForPublication:
        'Die Lizenz erfordert eine explizite Zustimmung zur Veröffentlichung seitens des:der Autor:in.',
      requiresCredits:
        'Die Lizenz erfordert die Nennung des:der Autor:in. Bitte gib deren Namen an.',
    },
  },

  saveIndicator: {
    label: 'Wird gespeichert',
  },

  showCreator: {
    title: 'Neue Sendereihe erstellen',
    missingShowTypes:
      'Es wurden noch keine Arten von Sendereihen angelegt. Du kannst das im <a href="%{adminUrl}/program/type/add/" target="_blank">Administrationsbereich</a> nachholen.',
    missingShowFundingCategories:
      'Es wurden noch keine Förderkategorien angelegt. Du kannst das im <a href="%{adminUrl}/program/fundingcategory/add/" target="_blank">Administrationsbereich</a> nachholen.',
  },

  conflictResolution: {
    title: 'Konfliktbehebung für neues Programm',
    titleModal: 'Sendeplatzkonflikt beheben für "%{show}"',

    titleNoConflict: 'Kein Konflikt vorhanden',
    noConflict: 'Dieser Sendeplatz hat keinen Konflikt. Wunderbar!',

    conflictingSlot: 'Neuer Sendeplatz',
    projectedSlot:
      'Der neue Sendeplatz läuft am <b>%{firstDate}</b> von <b>%{startTime}</b> bis <b>%{endTime}</b>',
    conflictsWith: 'Er steht im Konflikt mit den folgenden Sendeplätzen',

    newSchedule: 'von <b>%{firstDate}, %{startTime}</b> bis <b>%{endTime}</b>',
    recurringSchedule:
      'Dieses Ausstrahlungs-Schema wiederholt sich <b>%{rrule}</b> bis <b>%{lastDate}</b>',
    leftToResolve:
      'Noch %{smart_count} Konflikt zu beheben |||| Noch %{smart_count} Konflikte zu beheben',
    noneLeftToResolve: 'Keine Konflikte mehr zu beheben!',
    applySolution: 'Lösung anwenden',

    from: 'Von',
    to: 'Bis',
    showName: 'Sendungsname',

    whichStrategy: 'Welche Lösung sollen wir anwenden?',
    strategies: {
      ours: 'Neuen Sendeplatz einfügen, existierenden löschen',
      theirs: 'Neuen Sendeplatz löschen, existierenden behalten',
      theirsStart: 'Neuen Sendeplatz nach existierendem Sendeplatz einfügen',
      theirsEnd: 'Neuen Sendeplatz vor existierendem Sendeplatz einfügen',
      theirsBoth: 'Neuen Sendeplatz mit Unterbrechung durch alten Sendeplatz einfügen',
      oursStart: 'ours-start (TODO: Bessere Beschreibung)',
      oursEnd: 'ours-end (TODO: Bessere Beschreibung)',
      oursBoth: 'ours-both (TODO: Bessere Beschreibung)',
    },
  },

  paginator: {
    gotoFirst: 'Gehe zur ersten Seite',
    gotoPrev: 'Gehe zur vorherigen Seite',
    goto: 'Gehe zu Seite %{page}',
    gotoNext: 'Gehe zur nächsten Seite',
    gotoLast: 'Gehe zur letzen Seite',
    range: 'Zeige %{start} bis %{end} von %{count} Einträgen.',
  },

  scheduleEditor: {
    titleCreate: 'Neues Austrahlungs-Schema für "%{show}" erstellen',
    titleEdit: 'Austrahlungs-Schema der Sendunge "%{show}" bearbeiten',

    timeslotRuns:
      'Dieser Sendeplatz läuft am <b>%{firstDate}</b> von <b>%{startTime}</b> bis <b>%{endTime}</b>',
    recurringSchedule: 'Dieses Programm wiederholt sich <b>%{rrule}</b> bis <b>%{lastDate}</b>',

    singleEmission: 'Der Sendeplatz ist eine einmalige Ausstrahlung',
    coexistingTimeslot:
      'Aber aufgrund einer Konfliktbehebung gibt es einen weiteren Sendeplatz am <b>%{firstDate}</b> von <b>%{startTime}</b> bis <b>%{endTime}</b>',

    addFallbackTitle: 'Fallback für das Ausstrahlungs-Schema hinzufügen',
    currentlySelectedPlaylist: 'Momentan hinterlegte Playlist',
    assignPlaylist: 'Playlist zuweisen',

    start: 'Start',
    end: 'Ende',
    projectedDuration: '<b>Geplante Dauer:</b> %{duration}',

    pastEventWarning:
      'Vergangene Daten werden ignoriert. Startdatum wurde auf heute gesetzt!<br>Versuch es noch einmal oder setze ein Startdatum in die Zukunft.',
    from: 'Von',
    to: 'Bis',
    fromDate: 'Ab',
    toDate: 'Bis <span class="tw-font-normal tw-text-gray-500">(Datum)</span>',

    addRepetition: 'Wiederholungsprogramm hinzufügen',
    whenToRepeat: 'Wann soll wiederholt werden?',
    useSameTime: 'Selbe Start- und Endzeit verwenden',
    repeatAt: 'Wiederholen um',
    addNoOfDays: 'Wie viele Tage später?',
    onlyBusinessDays: 'Nur Werktage zählen',

    delete: {
      delete: 'Löschen',
      timeslotsTitle: 'Sendeplätze löschen',
      both: 'Beide löschen',
      scheduleTimeslots: 'Programm und alle Sendeplätze löschen',
      timeslot: 'Sendeplatz löschen',
      allTimeslots: 'Alle zukünftigen Sendeplätze löschen',
    },

    repetition: {
      followingDay: 'Am nächsten Tag',
      followingBusinessDay: 'Am nächsten Werktag',
      numberOfDaysLater: 'Eine Anzahl von Tagen später',
    },
  },

  editingMetadata: {
    timeSpecific: 'am %{date} um %{time} Uhr',
    lastModified: 'Letzte Änderung %{time} von %{author}.',
    created: 'Angelegt %{time} von %{author}.',
  },

  calendar: {
    today: 'Heute',
    empty: 'Keine Playlist',
    fallback: 'Fallback-Playlist',
    repetition: 'Wiederholung',
    view: {
      day: 'Tagesansicht',
      week: 'Wochenansicht',
    },
    switchShow: 'Zur „%{show}“ Sendereihe wechseln',
    editTimeslot: 'Sendung bearbeiten',
    intermission: 'Programmunterbrechung',
    mismatchedLength: 'Unpassende Länge',
    playing: 'Spielt gerade',
  },

  // Etc
  metadata: {
    artist: 'Künstler',
    album: 'Album',
    title: 'Titel',
    source: 'Quelle',
    size: 'Dateigröße',
  },

  rrule: {
    rrule: 'Wiederholungsregel',

    rule: {
      1: 'einmalig',
      2: 'täglich',
      3: 'werktäglich',
      4: 'wöchentlich',
      5: 'zweiwöchentlich',
      6: 'vierwöchentlich',
      7: 'gerade Kalenderwoche',
      8: 'ungerade Kalenderwoche',
      9: 'Jede 1. Woche im Monat',
      10: 'Jede 2. Woche im Monat',
      11: 'Jede 3. Woche im Monat',
      12: 'Jede 4. Woche im Monat',
      13: 'Jede 5. Woche im Monat',
    },
  },

  steeringErrorCodes: {
    'no-start-after-end': 'Das Startdatum darf nicht hinter dem Enddatum liegen.',
    'no-same-day-start-and-end': 'Start- und Enddatum dürfen nicht auf den selben Tag fallen.',
    'one-solution-per-conflict':
      'Es wird genau eine Lösung für jeden Konflikt benötigt. Es wurde entweder keine oder zuviele Lösungen für einen Konflikt übermittelt.',
  },
}
