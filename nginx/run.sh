#!/usr/bin/env sh

set -eu

# When we get killed, kill all our children
trap "exit" INT TERM
trap "kill 0" EXIT

AURA_ORIGIN=$AURA_PROTO://$AURA_HOST
SOURCE_DIR=/usr/share/nginx/html

envsubst "$(env | awk -F = '{printf \" $$%s\", $$1}')" < /etc/nginx/conf.d/nginx.template > /etc/nginx/conf.d/default.conf
# replace any occurrences of the specified variables in all text files in $SOURCE_DIR
find "$SOURCE_DIR" -type f -exec grep -Iq . {} \; -print | while read -r filename; do
  sed -i "s|__AURA_ORIGIN__|$AURA_ORIGIN|g" "$filename"
  sed -i "s|__AURA_HOST__|$AURA_HOST|g" "$filename"
  sed -i "s|__AURA_PROTO__|$AURA_PROTO|g" "$filename"
  sed -i "s|__DASHBOARD_OIDC_CLIENT_ID__|$DASHBOARD_OIDC_CLIENT_ID|g" "$filename"
done

echo "generated nginx-config"
cat /etc/nginx/conf.d/default.conf
nginx -g "daemon off;"
