# dashboard

> The web UI component of the AuRa framework

## Setup

### Prerequisites

This version is an early development prototype.

Detailed setup infos will follow, as soon as we reach something between alpha
and beta stage.

For dashboard to run locally or to deploy it to some webspace, you need
[Node.js](https://nodejs.org) and the Node Package Manager (`npm`), which comes
with Node.js. You have different [download options for Node.js](https://nodejs.org/en/download/)
depending on your operating system. You need at least Node.js v14.

### Configuration

All global configuration settings of the dashboard application can be set in the
corresponding `.env.*` files. You can use different settings for a `development`
and a `production` environment. So for a production environment you will have to
set all values in `.env.production`. For development use `.env.development`. For
more infos on environment variables, also for test and staging modes, see [Vue
CLI 3: Environment Variables and Modes](https://cli.vuejs.org/guide/mode-and-env.html)

This repository already provides sample environments in the files
_sample.env.development_ and _sample.env.production_, so you can copy them
to _.env.development_ and _.env.production_ depending on whether you want to
set up a dev or prod environment.

All needed values are provided with comments in the `sample.env.production`
file, so you can just take a look there and create your copy for the development
environment. There are some important notes on what to set and what to not
forget. For developers: Also be aware that these settings become environment
variables once compiled by _Vue.js_, so hot reload does not work, when you
change them. You have to compile the app again for changes to take effect.

For the _OpenID Connect_ settings it is very important to use exactly the same
redirect URIs as defined in you OIDC client settings in the _aura/steering_
module. So `VUE_APP_API_STEERING_OIDC_REDIRECT_URI` and
`VUE_APP_API_STEERING_OIDC_REDIRECT_URI_SILENT` should ideally be a copy-paste
from there.

For further information, check out the [detailed documentation on OIDC configuration](https://gitlab.servus.at/aura/aura/-/blob/master/docs/administration/oidc-client-config.md).

### Running it locally (development environment)

After you have set up the environment you first need to install the required packages:

```bash
npm install
```

And start the development server:

```bash
./run.sh
```

For customizing the whole Vue CLI development environment, see [Configuration
Reference](https://cli.vuejs.org/config/).

### Setting it up on a server (production environment)

Before building the final client code, you have to copy the
`sample.env.production` file to `.env.production` and change the values to your
final setup, as described in the _Configuration_ section above.

Similar to the dev environment you first have to install all dependencies by
`npm install`. Then you can build the production code, use `npm run build`. This
compiles and minifies the code for production. Then you just have to put the
contents of the _dist_ folder on some webspace. From a web server's perspective
this is just static code, so nothing special is needed except a plain web server
(using HTTPS with Let's Encrypt or any other TLS certificates is highly
recommended).

> TODO: provide an example setup

### Building/running with Docker

You can use Docker either to run the local development server, if you do not
want to set up node/npm on your host, or you can use Docker to just build the
_dist_ directory, that you then use in your dashboard's web root.

Before you build any image and run containers, make sure you followed the
steps above in the _Configuration_ section.

To create the image for the dev container and run it use the following:

```bash
docker build -t dashboard-dev --target=dev .
docker run --rm -it -v $(pwd):/aura -p 8080:8080 dashboard-dev
```

To use the build image to build the _dist_ directory do the following:

```bash
docker build -t dashboard --target=prod .
docker run --rm -it -v "$(pwd)"/dist_docker:/aura/dist_docker dashboard \
  sh -c 'npm run build && mv dist/* dist_docker'
```

In this case the final result will be put into the `dist_docker` directory, so
you can have different builds through docker and a local dev setup.

If you do not use any local dev setup and want to get the built files directly
into the `dist` directory, just change the `"$(pwd)"/dist_docker` in the volume
option to `"$(pwd)"/dist`. (If you are wondering why inside the container we
copy everything from the `dist` to the `dist_docker` container: this is a
workaround, because the build process needs the `dist` directory unlocked,
which is not the case if we bind it to a volume)

A more comprehensive setup in combination with other AuRa components can be
found in the [aura repository](https://gitlab.servus.at/aura/aura).

## Common problems and solutions

### Access to steering is denied due to CORS issues

For the dashboard to run in a dev mode you only need the `npm install` and `npm run dev` commands. To access show data in the show manager you also have to have
the [steering module](https://gitlab.servus.at/aura/steering) running somewhere.
There you need to add the following lines to the `pv/local_settings.py`, in
order to allow CORS requests from your dashboard:

```
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = (
	'localhost:8080'
)
```

This assumes of course that you have the dashboard running on its standard
localhost port 8080. If you want to change this to e.g. port `9090`, add a line
`PORT: 9090,` to the `.env.development` file of the dashboard package.

### Authentication is not working due to redirect issues

Dashboard can only be used with working authentication against the OIDC provider,
which is AuRa steering. Therefore, you have to set up an OpenID Connect client
there first. Additionally, you will also need a separate client for tank.

The standard setup for the dashboard client should use these values:

- _Client Type_: `Public`
- _Response Type_: `id_token token (Implicit Flow)`
- _JWT Algorithm_: `RS256`
- _Require Consent?_: `No`
- _Reuse Consent?_: `Yes`

You also have to set the _Redirect URIs_, which have to **match exactly** the
ones you configure in your _.env.development_ or _.env.production_ files here
in the dashboard source. This also means that if you use _localhost_ in steering,
you must not put _127.0.0.1_ or any equivalent in your dashboard config, but
use exactly the same string (and vice versa).

So if for example the _Redirect URIs_ in the steering OIDC client are set to:

```
http://localhost:8080/static/oidc_callback.html
http://localhost:8080/static/oidc_callback_silentRenew.html
```

Then your _.env.development_ (assuming this a dev environment) should contain:

```
VUE_APP_API_STEERING_OIDC_REDIRECT_URI = http://localhost:8080/oidc_callback.html
VUE_APP_API_STEERING_OIDC_REDIRECT_URI_SILENT = http://localhost:8080/oidc_callback_silentRenew.html
```

## Software Stack

This project is built with [Vue.js](https://vuejs.org). Take a look at the
[Guide](https://vuejs.org/guide/) or the [API docs](https://vuejs.org/api/)
to find out more about the core framework.

On top of Vue we use [vue-router](http://router.vuejs.org/) for in-app routing,
[BootstrapVue](https://bootstrap-vue.org/) for most of the generic UI components,
[Tailwind CSS](https://tailwindcss.com/) for custom component styling,
and [Vuex](https://vuex.vuejs.org/) for state management (with the latter most likely
being subject to change in the future).

We use [Vite](https://vitejs.dev/) as build system and dev server with little
custom configuration (see `vite.config.js`).

## Contributing

A lot of the code is still using the legacy Options API of Vue 2, which is possible
through Vue v3’s compat mode (see [@vue/compat](https://www.npmjs.com/package/@vue/compat)).
Feel free to re-implement components with the
[Composition API](https://vuejs.org/guide/introduction.html#composition-api).
New code should always use the Composition API.

If you’re in need for components or UX patterns that you’ve seen on other sites
or seem generic to you, it’s always a good idea to look have a look at
[VueUse](https://vueuse.org/) which comes with a lot of composables that are
handy in everyday UI development. Unstyled components are available from
[headless ui](https://headlessui.com/) if you need a specific UI component but need
a custom design. For more generic ecosystem information you should visit
[awesome-vue](https://github.com/vuejs/awesome-vue) and
[awesome-vue-3](https://github.com/vuesomedev/awesome-vue-3). If you add any new
dependencies make sure they don’t rely on any behaviours from Vue 2 as we are in
the process of migrating to a Vue 3-only setup.

Run `npm run format` to automatically format your code or run `npm run lint`
to check it for errors. ESLint emits some warnings for existing code.
Please make sure that you don’t add code that produces any new warnings.
