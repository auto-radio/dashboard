-include scripts/make/base.Makefile
-include scripts/make/docker.Makefile

help::
	@echo "$(APP_NAME) targets:"
	@echo "    init.app        - init application environment"
	@echo "    init.dev        - init development environment"
	@echo "    lint            - verify code style"
	@echo "    spell           - check spelling of text"
	@echo "    format          - apply automatic formatting"
	@echo "    build           - build a production bundle"
	@echo "    run             - start app in development mode"
	@echo "    run.prod        - start app in production mode"
	@echo "    release         - tag and push release with current version"
	$(call docker_help)

# Settings
DIR_BUILD ?= build
DIR_DIST ?= dist

PRETTIER = node_modules/.bin/prettier
ESLINT = node_modules/.bin/eslint
OPENAPI_TO_TS = scripts/convert-openapi-to-typescript.mjs
ifeq (, $(shell which docker-compose))
DOCKER_COMPOSE = docker compose
else
DOCKER_COMPOSE = docker-compose
endif

TANK_SCHEMA_URL ?= https://api.aura.radio/tank/api.json
TANK_SCHEMA_FILE = $(DIR_BUILD)/tank-schema.yaml
TANK_TYPES_FILE = src/tank-types.ts
STEERING_SCHEMA_URL ?= https://api.aura.radio/steering/api.json
STEERING_SCHEMA_FILE = $(DIR_BUILD)/steering-schema.yaml
STEERING_TYPES_FILE = src/steering-types.ts

NPM_CI_ARGS ?=

VERSION = $(shell npm pkg get version | sed 's/"//g')
SOURCE_FILES = \
	index.html \
	oidc_callback.html \
	oidc_callback_silentRenew.html \
	$(shell find src public -type f)
SOURCE_CONFIG_FILES = \
	postcss.config.js \
	tailwind.config.js \
	tsconfig.json \
	vite.config.ts

$(DIR_DIST): node_modules $(SOURCE_FILES) $(SOURCE_CONFIG_FILES)
	npm run build -- --outDir "$@"
	touch --no-create "$@"
DOCKER_TARGET ?= "prod"
DOCKER_RUN = @docker run \
		--name $(APP_NAME) \
		--network="host" \
		--env-file .env.docker \
		-u $(UID):$(GID) \
		$(DOCKER_ENTRY_POINT) \
		autoradio/$(APP_NAME)

# Targets
init.app: node_modules
	cp sample.env.production .env.production

init.dev: node_modules
	cp sample.env.development .env.development
	cp sample.env.docker .env.docker

spell: node_modules
	npm run spell

format: node_modules
	npm run format

build: $(DIR_DIST)

run: node_modules
	npm run dev

run.prod: node_modules
	npm run serve -- --host 0.0.0.0

release:
	git tag $(VERSION)
	git push origin $(VERSION)
	@echo "Release '$(VERSION)' tagged and pushed successfully."

# ".FORCE" is just an arbitrary target name, which will never exist
.PHONY: .FORCE
.FORCE:
	@true

node_modules: package.json package-lock.json
	ADBLOCK=true npm ci $(NPM_CI_ARGS)
	touch --no-create node_modules

$(ESLINT) $(PRETTIER) $(OPENAPI_TO_TS): node_modules

$(SCHEMA_FILE): .FORCE
	@mkdir -p "$(dir $@)"
	if [ -f "$(SCHEMA_URL)" ]; then \
  		cp "$(SCHEMA_URL)" "$(SCHEMA_FILE)"; \
  	else \
		curl --silent --show-error --location --time-cond "$(SCHEMA_FILE)" --output "$(SCHEMA_FILE)" "$(SCHEMA_URL)"; \
	fi

.PHONY: update-types-from-schema
update-types-from-schema: $(TYPES_FILE)

$(TYPES_FILE): $(SCHEMA_FILE) $(ESLINT) $(PRETTIER)
	$(OPENAPI_TO_TS) "$(SCHEMA_FILE)" "$@"
	$(PRETTIER) --write "$(TYPES_FILE)" || true
	$(ESLINT) --no-ignore --fix "$(TYPES_FILE)" || true

.PHONY: update-types-steering
update-types-steering:
	$(MAKE) update-types-from-schema "TYPES_FILE=$(STEERING_TYPES_FILE)" "SCHEMA_FILE=$(STEERING_SCHEMA_FILE)" "SCHEMA_URL=$(STEERING_SCHEMA_URL)"

.PHONY: update-types-tank
update-types-tank:
	$(MAKE) update-types-from-schema "TYPES_FILE=$(TANK_TYPES_FILE)" "SCHEMA_FILE=$(TANK_SCHEMA_FILE)" "SCHEMA_URL=$(TANK_SCHEMA_URL)"

.PHONY: update-types
update-types: update-types-steering update-types-tank

.PHONY: lint
lint: node_modules
	npm run lint -- --no-fix

.PHONY: test-local
test-local: $(DIR_DIST)
	npx playwright test --project chromium

.PHONY: test
.ONESHELL:
test:
	. tests/env
	$(DOCKER_COMPOSE) \
		--env-file tests/env \
		--file tests/docker-compose.yml \
		up \
		--pull always \
		--build \
		--force-recreate \
		--exit-code-from dashboard-test \
	;
