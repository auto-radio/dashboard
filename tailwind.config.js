// Note that any adjustments to this file must be reflected in custom.scss and tailwind.css
const plugin = require('tailwindcss/plugin')

module.exports = {
  prefix: 'tw-',
  content: ['./src/**/*.{js,ts,vue}'],
  important: true,
  corePlugins: {
    preflight: false,
  },
  plugins: [
    plugin(function ({ addVariant, addUtilities }) {
      addVariant('hocus', ['&:hover', '&:focus-visible'])
      addVariant('group-hocus', ':merge(.group):is(:hover, :focus-visible) &')
      addUtilities({
        '.inset-y-center': {
          'inset-block': 0,
          'margin-block': 'auto',
        },
        '.inset-x-center': {
          'inset-inline': 0,
          'margin-inline': 'auto',
        },
        '.inset-center': {
          inset: 0,
          margin: 'auto',
        },
      })
    }),
  ],
  theme: {
    screens: {
      sm: '576px',
      md: '768px',
      lg: '992px',
      xl: '1200px',
    },
    extend: {
      colors: {
        aura: {
          purple: '#5029c4',
          primary: '#007bff',
        },
        gray: {
          50: '#f9fafb',
          100: '#f4f5f7',
          200: '#e5e7eb',
          300: '#d2d6dc',
          400: '#9fa6b2',
          500: '#6b7280',
          600: '#4b5563',
          700: '#374151',
          800: '#252f3f',
          900: '#161e2e',
        },
      },
    },
  },
}
