#!/usr/bin/env node

import { writeFile } from 'fs/promises'
import openapiTS from 'openapi-typescript'

function convertOpenapiToTypescript(inputFilePath) {
  const time = new Date().toISOString().split('T').join(' ')
  return openapiTS(inputFilePath, {
    commentHeader: `
/* eslint-disable */
/*
 * This file was auto-generated by \`make update-types\` at ${time}.
 * DO NOT make changes to this file.
 */
        `.trimStart(),
    transform(schemaObject, metadata) {
      // This is a bug in the tank API schema generation. SourceURL is actually just a string.
      if (metadata.path === '#/components/schemas/importer.SourceURL') {
        return 'string'
      }
    },
  })
}

convertOpenapiToTypescript(process.argv[2])
  .then((types) => {
    return writeFile(process.argv[3], types)
  })
  .catch((e) => {
    console.error('Unable to generate types for input', e)
  })
