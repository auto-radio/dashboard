import { defineConfig, devices } from '@playwright/test'

const DIR_DIST = process.env.DIR_DIST
const SERVER_URL = process.env.DASHBOARD_SERVER_URL
const DEFAULT_SERVER_PORT = parseInt(process.env.DASHBOARD_PORT ?? '27142')
const DEFAULT_SERVER_URL = `http://127.0.0.1:${DEFAULT_SERVER_PORT}`

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export default defineConfig({
  globalSetup: require.resolve('./tests/setup'),
  testDir: './tests',
  /* Maximum time one test can run for. */
  timeout: 30 * 1000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 5000,
  },
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 2 : 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: [['html', { open: 'never' }]],
  use: {
    actionTimeout: 0,
    baseURL: SERVER_URL ?? DEFAULT_SERVER_URL,
    storageState: 'session.json',
    screenshot: 'only-on-failure',
    video: 'retain-on-failure',
    trace: 'retain-on-failure',
  },
  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'firefox',
      use: { ...devices['Desktop Firefox'] },
    },
    {
      name: 'webkit',
      use: { ...devices['Desktop Safari'] },
    },
  ],
  ...(!SERVER_URL
    ? {
        webServer: {
          command: `npm run preview -- --outDir '${DIR_DIST}' --port ${DEFAULT_SERVER_PORT} --strictPort`,
          port: DEFAULT_SERVER_PORT,
        },
      }
    : {}),
})
