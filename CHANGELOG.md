# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0-alpha2] - 2023-06-20

### Added

- a new show selector to the navbar (#122, f9762d85).
- a new image picker that can be used to upload new and select existing images (#89, #139, e0fd0a76, b335d801, 57e1e10f).
- ability to add internal notes to a show (#146, 319cf540).
- information on empty slots between shows and a progress indicator for the currently playing show
  to the calendar day-view (#155, 9ac48a55, 2a68a7ac).
- a progress indicator for the currently playing show to the calendar week-view (#156, b52672dd).
- ability to add and remove individual contributors to the show notes based on the show owners (#170, #146, aa707763).
- ability to add tags to show notes (#146, 560d8dda).

### Fixed

- Navigation would sometimes freeze, after an application error in the playlists view (#132, e4083e66).
- Calendar would sometimes crash if playlist data is missing (#132, a3e8e3b0).
- Switching between the week- and day-view in the calendar no longer squishes the week-view (#153, e28eb4f7).
- The calendar uses entire viewport height now (#154, 4c90f277).
- Users are now able to create a timeslot in a calendar week slot that has already started (#167, 3d4e9b28).
- HTML in the show title, short description and description is now correctly rendered (#129, 649b4c0b).

### Changed

- The timeslot overview has been fully revised (4ef88279).
- The timeslot show notes editor has been fully revised (#141, 57e1e10f).
- Past schedules are now hidden in the show manager (#120, 8cd743de).
- Schedules are now shown with their respective start and end date (#121, f9dc6fef).
- The calendar day-view has been fully revised (#155, 9ac48a55, 2a68a7ac).
- The slots in the calendar week-view have been from 30 to 15 minutes to make it easier to
  create shorter slots (#151, 3fc47383).
- Very short timeslots in the calendar week-view now take up less space (#151, 3fc47383, bfe7f813).
- Show start and end time in calendar tooltip if the event is too short to render it as content (#173, cf5ac12e).
- The show manager interface for editing the show details has been overhauled, changing the overall appearance
  and the way some of the fields are edited (db7dc49d).

### Removed

- Non-functional nav button to broadcast management on show detail page (#133, e4083e66).

## [1.0.0-alpha1] - 2023-02-27

Initial release.
