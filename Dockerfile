FROM node:lts-alpine AS base
WORKDIR /dashboard
COPY package*.json ./
RUN ADBLOCK=true npm ci

FROM base AS dev
EXPOSE 8080
VOLUME ["/dashboard"]
CMD ["./docker_run.sh"]

FROM base AS prod_builder
COPY . .
# Vite will auto-source .env* files
# see: https://vitejs.dev/guide/env-and-mode.html#env-files
RUN mv docker.env.production .env.production
RUN npm run build

FROM nginx:stable-alpine AS prod
COPY nginx/nginx.prod.template /etc/nginx/conf.d/nginx.template
COPY nginx/run.sh /run.sh
COPY --from=prod_builder /dashboard/dist /usr/share/nginx/html
CMD /run.sh
